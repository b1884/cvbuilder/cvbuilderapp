const express = require('express');
const path = require('path');
const compression = require('compression');
const app = express();

// Serve the static files from the React app
app.use(compression({ level: 9 }));
app.use(express.static(path.join(__dirname, '/dist/cvbuilderapp/')));

// Handles any requests that don't match the ones above
app.get('*', (req,res) => {
    res.sendFile(path.join(__dirname+'/dist/cvbuilderapp/index.html'));
});

const port = process.env['CVBUILDER_APP_PORT'] || 3698;
app.listen(port, args => {
  console.log("Front-end server for cvbuilder app running on port ", port, '!');
});
