cd ~/cv-template.co/cvbuilderapp
git checkout master
git pull origin master

NODE_ENV="whatever_not_prod" yarn install
yarn prod:build
pm2 restart cvbuilderapp || pm2 start ~/cv-template.co/cvbuilderapp/server.js --name "cvbuilderapp"
