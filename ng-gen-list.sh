#!/bin/bash
if [ "$1" == "--m" ]; then
for modname in $3
do
  npx ng g m $2/$modname
  done;
elif [ "$1" == "--c" ]; then
for compname in $4
do
  npx ng g c "$2/$3/$compname" --skip-tests
  done;
fi
