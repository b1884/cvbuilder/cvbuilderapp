export interface VerifyEmailPayloadInterface {
	key: string;
}

export interface SignInPayloadInterface {
	email: string;
}

export interface SignInResponseInterface {
	iat: number;
	exp: number;
}

export interface SignUpResponseInterface extends SignInResponseInterface {
	resumeLink: string;
}
