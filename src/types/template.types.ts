import { LangType } from './lang.types';

export interface TemplateInterface {
	_id?: string;
	name: string;
	codeName: string;
	previewSm: string;
	previewXl: string;
	lang: LangType;
	selected?: boolean;
	creationDate?: Date;
	lastUpdateDate?: Date;
}
