import { LangType } from './lang.types';
import { SectionCodeType } from '../features/builder/pages/builder-page/builder-form.modal';

export interface SlotInterface {
	name?: string;
	slotId?: string;
	link?: string;
	skill?: string;
	lang?: string;
	level?: string;
	postTitle?: string;
	host?: string;
	duration?: string;
	durationEnd?: string;
	city?: string;
	description?: string;
	index?: string;
}

export interface FieldValueInterface {
	label?: string;
	value?: string;
	show?: boolean;
}

export interface FieldInterface {
	title: string;
	customTitle?: string;
	code: SectionCodeType;
	desiredPostTitle?: string;
	firstName?: string;
	lastName?: string;
	email?: string;
	phone?: string;
	country?: string;
	city?: string;
	address?: string;
	zipCode?: string;
	drivingLicense?: string;
	nationality?: string;
	birthLocation?: string;
	birthDate?: string;
	personalResume?: string;
	slots?: SlotInterface[];
	[key: string]: FieldValueInterface | string | SlotInterface[] | undefined;
}

export interface ResumeInputInterface {
	title: string;
	stylesheetLink?: string;
	language: LangType;
	fields: FieldInterface[];
}
