import { LangType } from './lang.types';
import { ResumeInputInterface } from './resume-input.types';

export interface ClassicResumeInterface {
	_id?: string;
	userId: string;
	templateCodeName: string;
	title: string;
	link: string;
	lang: LangType;
	sharingLink: string;
	isShareable?: string;
	rawData: ResumeInputInterface;
	profileImage?: string;
	profileImageLink?: string;
	previewImage?: string;
	lastUpdateDate: Date;
}

export type ClassicResumePreviewType = Omit<ClassicResumeInterface, 'rawData'>;

export declare type WriteImageResponseType = {
	name?: string;
	size?: string;
	path?: string;
	fileSize?: number;
	page?: number;
};

export interface ClassicGeneratorOutputInterface {
	previewImage: string;
	pagesAsImages: WriteImageResponseType[];
}
