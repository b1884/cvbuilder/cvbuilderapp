export interface UserInterface {
	_id?: string;
	name: string;
	lastName: string;
	email: string;
	unConfirmedEmail?: string | null;
	subscriptionType?: string;
	subscriptionStartTime?: Date;
	creationDate?: Date;
}
