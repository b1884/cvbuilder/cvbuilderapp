import { environment } from '../../environments/environment';

export const getBuilderImageURL = (userId: string, resumeId: string, name: string): string =>
	`${environment.builderApi}/classic_media/${userId}/${resumeId}/images/${name}`;

export const getPersonalPhotoURL = (userId: string, fileName: string): string =>
	`${environment.builderApi}/personal_photos/${userId}/${fileName}`;

export const getTemplatePhotoURL = (templateLink: string): string =>
	`${environment.builderApi}/classic_previews/${templateLink}`;
