const LEGAL_SIZE = 5242880; // We only 5megabytes or less.
const LEGAL_TYPES = ['image/png', 'image/jpg', 'image/jpeg'];

export interface BuilderImageSizeInterface {
	width: number;
	height: number;
}

export const calculateAspectRatioFit = (
	srcWidth: number,
	srcHeight: number,
	maxWidth: number,
	maxHeight: number
): BuilderImageSizeInterface => {
	const heightBarrier = maxHeight > 1050 ? 1050 : maxHeight;
	const widthBarrier = maxWidth > 888 ? 888 : maxHeight;
	const ratio = Math.min(widthBarrier / srcWidth, heightBarrier / srcHeight);

	return { width: srcWidth * ratio, height: srcHeight * ratio };
};

export const percentOf = (percentage: number, totalValue: number): number => (percentage / 100) * totalValue;

const formatBytes = (bytes: number, decimals = 2): string => {
	if (bytes === 0) return '0 Bytes';

	const k = 1024;
	const dm = decimals < 0 ? 0 : decimals;
	const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

	const i = Math.floor(Math.log(bytes) / Math.log(k));

	return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`;
};

export const isFileLegal = (file: File, sizeErrMsg: string, formatErrMsg: string): boolean => {
	if (file.size > LEGAL_SIZE) {
		alert(`${sizeErrMsg} ( ${formatBytes(file.size)} > ${formatBytes(LEGAL_SIZE)} )`);
		return false;
	}

	if (!LEGAL_TYPES.includes(file.type)) {
		alert(`${formatErrMsg} [jpeg / jpg / png] `);
		return false;
	}

	return true;
};
