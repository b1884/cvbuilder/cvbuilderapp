const characters = '0123456789abcdefghijklmnopqrstuvwxyz!@#$%^&*()ABCDEFGHIJKLMNOPQRSTUVWXYZ';

export const generateRandomText = (): string => {
	const phrase = {
		text: ''
	};

	for (let i = 0; i <= 12; i++) {
		const randomNumber = Math.floor(Math.random() * characters.length);
		phrase.text += characters.substring(randomNumber, randomNumber + 1);
	}

	return phrase.text;
};
