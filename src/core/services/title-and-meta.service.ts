import { Inject, Injectable } from '@angular/core';
import { Meta, MetaDefinition, Title } from '@angular/platform-browser';
import { DOCUMENT, Location } from '@angular/common';
import { environment } from '../../environments/environment';

@Injectable({
	providedIn: 'root'
})
export class TitleAndMetaService {
	private _appName = environment.appName;
	private _mainUrl = `${window.location.protocol}//${window.location.host}`;

	constructor(
		private _metaService: Meta,
		private _titleService: Title,
		private _loc: Location,
		@Inject(DOCUMENT) private _doc: Document
	) {}

	upsertMetaTag(tag: MetaDefinition): void {
		const selector = tag.name ? 'name' : 'property';
		const exists = this._metaService.getTag(`${selector}="${tag[selector as keyof MetaDefinition]}"`);

		if (exists) {
			this._metaService.updateTag(tag);
			return;
		}

		this._metaService.addTag(tag);
		return;
	}

	createLinkForCanonicalURL(): void {
		const url = `${this._mainUrl}${this._loc.path()}`;

		const existingLink = this._doc.querySelector('link[rel="canonical"]');

		if (existingLink) {
			existingLink.setAttribute('href', url);
		} else {
			const link: HTMLLinkElement = this._doc.createElement('link');
			link.setAttribute('rel', 'canonical');
			this._doc.head.appendChild(link);
			link.setAttribute('href', url);
		}

		this.upsertMetaTag({
			property: 'og:url',
			content: url
		});
	}

	setTitle(title: string, addWebsiteName = true, separator = '|'): void {
		const titleText = addWebsiteName ? `${this._appName} ${separator} ${title}` : title;

		this._titleService.setTitle(titleText);
		this.upsertMetaTag({
			name: 'title',
			content: titleText
		});
		this.upsertMetaTag({
			name: 'og:title',
			content: titleText
		});
	}

	initPageMeta(
		title: string,
		description: string,
		keywords: string,
		imagePath?: string,
		addWebsiteName = true,
		titleSeparator = '|'
	): void {
		this.upsertMetaTag({
			property: 'og:image',
			content: imagePath ? imagePath : `${this._mainUrl}/assets/banner/cv-template.webp`
		});

		this.upsertMetaTag({
			name: 'description',
			content: description
		});

		this.upsertMetaTag({
			name: 'keywords',
			content: keywords
		});

		this.upsertMetaTag({
			name: 'og:description',
			content: description
		});

		this.setTitle(title, addWebsiteName, titleSeparator);
		this.createLinkForCanonicalURL();
	}
}
