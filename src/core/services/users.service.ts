import { Injectable } from '@angular/core';
import { NetworkService } from './network.service';
import { UserInterface } from '../../types/user.type';
import { ResponseInterface } from '../../types/response.types';
import { firstValueFrom } from 'rxjs';
import { getUsersURL } from '../config/api-routes.config';

@Injectable({
	providedIn: 'root'
})
export class UsersService {
	private _currentUser!: UserInterface | null;

	constructor(private _http: NetworkService) {}

	get currentUser(): UserInterface | null {
		return this._currentUser;
	}

	set currentUser(value: UserInterface | null) {
		this._currentUser = value;
	}

	async currentUserAPI(force = false): Promise<UserInterface | null> {
		if (!force && this._currentUser) {
			return this._currentUser;
		}

		const res: ResponseInterface<UserInterface> = await firstValueFrom<ResponseInterface<UserInterface>>(
			this._http.get<unknown, UserInterface>(getUsersURL('current'))
		);

		if (!res.success || !res.body) {
			return null;
		}

		this._currentUser = res.body;

		return res.body;
	}

	async updateUserAPI(userPayload: Partial<UserInterface>): Promise<void> {
		const res: ResponseInterface = await firstValueFrom<ResponseInterface>(
			this._http.patch<Partial<UserInterface>>(getUsersURL('update'), userPayload)
		);

		if (!res.success) {
			throw new Error(res.message);
		}

		return;
	}

	async purgeUserAPI(): Promise<void> {
		const res: ResponseInterface = await firstValueFrom<ResponseInterface>(
			this._http.delete(getUsersURL('purge'), null)
		);

		if (!res.success) {
			throw new Error(res.message);
		}

		return;
	}
}
