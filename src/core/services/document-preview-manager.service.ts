import { Injectable } from '@angular/core';
import { ClassicResumeInterface } from '../../types/classic-resume.types';
import { isEqual } from 'lodash';

export interface CurrentResumeJobInterface {
	content: ClassicResumeInterface;
	pdf?: ArrayBuffer;
}

@Injectable({
	providedIn: 'root'
})
export class DocumentPreviewManagerService {
	private _currentResumeJob!: CurrentResumeJobInterface;

	get currentResumeJob(): CurrentResumeJobInterface {
		return this._currentResumeJob;
	}

	set currentResumeJob(value: CurrentResumeJobInterface) {
		this._currentResumeJob = value;
	}

	shouldBuildAgain(content?: ClassicResumeInterface): boolean {
		if (!this.currentResumeJob) {
			return true;
		}

		if (!this.currentResumeJob.content) {
			return true;
		}

		if (!content && !this.currentResumeJob.pdf) {
			return true;
		}

		if (!content && this.currentResumeJob.pdf) {
			return false;
		}

		if (content && isEqual(this.currentResumeJob.content, content) && !this.currentResumeJob.pdf) {
			return true;
		}

		if (content && isEqual(this.currentResumeJob.content, content) && this.currentResumeJob.pdf) {
			return false;
		}

		if (content && !isEqual(this.currentResumeJob.content, content)) {
			return true;
		}

		return true;
	}
}
