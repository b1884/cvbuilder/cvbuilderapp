import { Injectable } from '@angular/core';
import {
	HttpRequest,
	HttpHandler,
	HttpEvent,
	HttpInterceptor,
	HttpErrorResponse,
	HttpResponse
} from '@angular/common/http';
import { catchError, Observable, of, throwError } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { UsersService } from './users.service';
import appRoutes from '../../routes';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class InOutInterceptor implements HttpInterceptor {
	constructor(
		private _cookieService: CookieService,
		private _userService: UsersService,
		private _translate: TranslateService
	) {}

	intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
		request = request.clone({
			setHeaders: {
				'x-client-language': `${this._translate.currentLang || this._translate.defaultLang}`
			}
		});

		return next.handle(request).pipe(
			catchError((error: HttpErrorResponse) => {
				switch (error.status) {
					case 403:
						this._cookieService.deleteAll();
						this._userService.currentUser = null;
						location.href = appRoutes.facade.home;
						return of(new HttpResponse({ status: 200, body: { success: false } }));
					default:
						return throwError(() => error);
				}
			})
		);
	}
}
