import { Injectable } from '@angular/core';
import { NetworkService } from './network.service';
import { ResponseInterface } from '../../types/response.types';
import { firstValueFrom } from 'rxjs';
import { getBuilderURL } from '../config/api-routes.config';
import {
	ClassicGeneratorOutputInterface,
	ClassicResumeInterface,
	ClassicResumePreviewType
} from '../../types/classic-resume.types';
import { getBuilderImageURL, getPersonalPhotoURL, getTemplatePhotoURL } from '../utils/links.util';
import { TemplateInterface } from '../../types/template.types';
import { SignUpResponseInterface } from '../../types/auth.types';
import { environment } from '../../environments/environment';

@Injectable({
	providedIn: 'root'
})
export class BuilderService {
	currentResume!: ClassicResumeInterface;

	constructor(private _http: NetworkService) {}

	async getTemplatesListAPI(): Promise<TemplateInterface[] | null> {
		const res: ResponseInterface<TemplateInterface[]> = await firstValueFrom<
			ResponseInterface<TemplateInterface[]>
		>(this._http.get<unknown, TemplateInterface[]>(getBuilderURL('templates')));

		if (!res.success || !res.body) {
			return null;
		}

		return res.body.map((value: TemplateInterface) => ({
			...value,
			previewSm: getTemplatePhotoURL(value.previewSm),
			previewXl: getTemplatePhotoURL(value.previewXl)
		}));
	}

	async getPreviewsListAPI(): Promise<ClassicResumePreviewType[] | null> {
		const res: ResponseInterface<ClassicResumePreviewType[]> = await firstValueFrom<
			ResponseInterface<ClassicResumePreviewType[]>
		>(this._http.get<unknown, ClassicResumePreviewType[]>(getBuilderURL('getPreviewsList')));

		if (!res.success || !res.body) {
			return null;
		}

		return res.body.map(
			(value: ClassicResumePreviewType) =>
				({
					...value,
					previewImage: getBuilderImageURL(value.userId, value._id as string, value.previewImage as string)
				} as ClassicResumePreviewType)
		);
	}

	async currentResumeAPI(link: string): Promise<ClassicResumeInterface | null> {
		const res: ResponseInterface<ClassicResumeInterface> = await firstValueFrom<
			ResponseInterface<ClassicResumeInterface>
		>(
			this._http.get<Pick<ClassicResumeInterface, 'link'>, ClassicResumeInterface>(getBuilderURL('current'), {
				link
			})
		);

		if (!res.success || !res.body) {
			return null;
		}

		if (res.body.profileImage) {
			res.body.profileImageLink = getPersonalPhotoURL(res.body.userId, res.body.profileImage);
		}

		this.currentResume = res.body;
		return res.body;
	}

	async downloadResumeAPI(link: string, userId: string, resumeId: string): Promise<string[] | null> {
		const res: ResponseInterface<string> = await firstValueFrom<ResponseInterface<string>>(
			this._http.get<Pick<ClassicResumeInterface, 'link'>, string>(getBuilderURL('download'), {
				link
			})
		);

		if (!res.success || !res.body) {
			return null;
		}

		return [res.body, `${environment.builderApi}/classic_media/${userId}/${resumeId}/pdf/${res.body}`];
	}

	async updateResumeAPI(resume: Partial<ClassicResumePreviewType>): Promise<void> {
		const { profileImageLink, ...noImageResume } = resume;

		const res: ResponseInterface = await firstValueFrom<ResponseInterface>(
			this._http.patch<Partial<ClassicResumePreviewType>>(getBuilderURL('updateResume'), noImageResume)
		);

		if (!res.success) {
			throw new Error(res.message);
		}

		return;
	}

	async deleteResumeAPI(resume: Partial<ClassicResumePreviewType>): Promise<void> {
		const res: ResponseInterface = await firstValueFrom<ResponseInterface>(
			this._http.delete<Partial<ClassicResumePreviewType>>(getBuilderURL('deleteResume'), resume)
		);

		if (!res.success) {
			throw new Error(res.message);
		}

		return;
	}

	async removePersonalPhotoAPI(resume: Partial<ClassicResumePreviewType>): Promise<void> {
		const res: ResponseInterface = await firstValueFrom<ResponseInterface>(
			this._http.delete<Partial<ClassicResumePreviewType>>(getBuilderURL('removePhoto'), resume)
		);

		if (!res.success) {
			throw new Error(res.message);
		}

		return;
	}

	async updatePersonalPhotoAPI(
		resume: Partial<ClassicResumePreviewType>,
		file: File
	): Promise<Required<Pick<ClassicResumePreviewType, 'profileImage'>>> {
		const fullBody = { ...resume, file };

		if (fullBody.profileImage) {
			delete fullBody.profileImage;
		}

		const form = new FormData();
		const keys = Object.keys(fullBody);

		for (const key of keys) {
			form.set(key, fullBody[key as keyof Partial<ClassicResumePreviewType> & keyof { file: File }]);
		}

		const res: ResponseInterface<Required<Pick<ClassicResumePreviewType, 'profileImage'>>> = await firstValueFrom<
			ResponseInterface<Required<Pick<ClassicResumePreviewType, 'profileImage'>>>
		>(
			this._http.patch<
				Partial<ClassicResumePreviewType>,
				Required<Pick<ClassicResumePreviewType, 'profileImage'>>
			>(getBuilderURL('uploadPhoto'), form)
		);

		if (!res.success || !res.body) {
			throw new Error(res.message);
		}

		return res.body;
	}

	async buildAPI(resume: ClassicResumeInterface): Promise<ClassicGeneratorOutputInterface | null> {
		const res: ResponseInterface<ClassicGeneratorOutputInterface> = await firstValueFrom<
			ResponseInterface<ClassicGeneratorOutputInterface>
		>(this._http.post<ClassicResumeInterface, ClassicGeneratorOutputInterface>(getBuilderURL('build'), resume));

		if (!res.success || !res.body) {
			return null;
		}

		return res.body;
	}

	async buildPDFBufferAPI(resume: ClassicResumeInterface): Promise<ArrayBuffer | undefined> {
		const res: ArrayBuffer = await firstValueFrom<ArrayBuffer>(
			this._http.postBufferResp<ClassicResumeInterface>(getBuilderURL('buildPDFBuffer'), resume)
		);

		if (!res) {
			return undefined;
		}

		return res;
	}

	async createNewAPI(): Promise<Pick<SignUpResponseInterface, 'resumeLink'> | null> {
		const res: ResponseInterface<Pick<SignUpResponseInterface, 'resumeLink'>> = await firstValueFrom<
			ResponseInterface<Pick<SignUpResponseInterface, 'resumeLink'>>
		>(this._http.post<unknown, Pick<SignUpResponseInterface, 'resumeLink'>>(getBuilderURL('createNew'), null));

		if (!res.success || !res.body) {
			return null;
		}

		return res.body;
	}
}
