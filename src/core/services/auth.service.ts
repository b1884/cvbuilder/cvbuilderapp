import { Injectable } from '@angular/core';
import { NetworkService } from './network.service';
import { getAuthURL } from '../config/api-routes.config';
import { firstValueFrom } from 'rxjs';
import { ResponseInterface } from '../../types/response.types';

@Injectable({
	providedIn: 'root'
})
export class AuthService {
	constructor(private _http: NetworkService) {}

	async logoutAPI(): Promise<void> {
		const res: ResponseInterface = await firstValueFrom<ResponseInterface>(
			this._http.post(getAuthURL('signOut'), {})
		);

		if (!res.success) {
			throw new Error(res.message || 'Request failed.');
		}
		return;
	}

	async setLanguageAPI(): Promise<void> {
		const res: ResponseInterface = await firstValueFrom<ResponseInterface>(
			this._http.post(getAuthURL('setLanguage'), {})
		);

		if (!res.success) {
			throw new Error(res.message || 'Request failed.');
		}
		return;
	}
}
