export enum PubSubTopicsEnum {
	toggleMenu = 'toggleMenu',
	hamburgerEffects = 'hamburgerEffects',
	changeCvTemplate = 'changeCvTemplate'
}
