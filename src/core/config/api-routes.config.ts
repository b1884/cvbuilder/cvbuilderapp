import { environment } from '../../environments/environment';

const auth = {
	signUp: '/sign-up',
	signIn: '/sign-in',
	signOut: '/sign-out',
	verifyEmail: '/verify-email',
	setLanguage: '/set-language'
};

const builder = {
	build: '/build',
	buildPDFBuffer: '/build-pdf-buffer',
	createNew: '/create-new',
	getPreviewsList: '/previews-list',
	getResume: '/resume',
	updateResume: '/update-resume',
	deleteResume: '/delete-resume',
	download: '/download',
	current: '/current',
	templates: '/templates',
	uploadPhoto: '/upload-personal-photo',
	removePhoto: '/remove-personal-photo'
};

const subscription = {
	all: '/all'
};

const users = {
	single: '/single',
	current: '/current',
	update: '/update-user',
	purge: '/purge-user'
};

export const getAuthURL = (child: keyof typeof auth): string => `${environment.builderApi}/auth${auth[child]}`;

export const getUsersURL = (child: keyof typeof users): string => `${environment.builderApi}/users${users[child]}`;

export const getBuilderURL = (child: keyof typeof builder): string =>
	`${environment.builderApi}/classic-builder${builder[child]}`;

export const getSubscriptionURL = (child: keyof typeof subscription): string =>
	`${environment.builderApi}/subscription${subscription[child]}`;
