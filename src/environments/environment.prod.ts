export const environment = {
	production: true,
	appName: 'CV Template',
	mainURL: 'https://cv-template.co',
	builderApp: 'https://app.cv-template.co',
	builderApi: 'https://suwoxoxoti22.cv-template.co/api',
	blog: 'https://cv-template.co/blog'
};
