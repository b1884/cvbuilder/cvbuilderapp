import dashboard from './dashboard.fr.locals';
import common from './common.fr.locals';
import modals from './modals.fr.locals';
import builder from './builder.fr.locals';

export default {
	dashboard,
	common,
	modals,
	builder
};
