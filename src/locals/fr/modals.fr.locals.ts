export default {
	shareLink: {
		title: 'Partagez un lien vers votre CV',
		description:
			"Copiez et collez l'URL pour envoyer votre CV par SMS, e-mail ou pour partager votre CV sur votre site personnel.",
		linkLabel: 'Copier cette URL privée',
		buttons: {
			openLink: 'Ouvrir le lien',
			copyLink: 'Copier le lien'
		}
	},
	deleteCv: {
		title: 'Supprimer le CV',
		description: 'Êtes-vous sûr de vouloir effacer ce CV ? Une fois supprimé, ce CV ne peut plus être restauré.',
		confirm: 'Supprimer',
		cancel: 'Annuler'
	},
	deleteProfile: {
		title: 'Êtes-vous sûr de vouloir supprimer votre compte ?',
		description:
			'Une fois que vous cliquez sur supprimer, votre compte et les données associées seront supprimés définitivement ' +
			'et ne peuvent pas être restaurés. Alternativement, si vous gardez votre compte gratuit, ' +
			'la prochaine fois que vous souhaitez modifier ou mettre à jour votre CV, vous ne devrez pas repartir de zéro.',
		confirm: 'Supprimer',
		cancel: 'Conserver mon compte'
	},
	deletePhoto: {
		title: 'Supprimer la photo',
		description: 'Êtes-vous sûr de vouloir effacer ce photo ?',
		confirm: 'Supprimer',
		cancel: 'Annuler'
	},
	deleteSlot: {
		title: "Effacer l'entrée",
		description: 'Êtes-vous sûr de vouloir supprimer cette entrée ?',
		confirm: 'Supprimer',
		cancel: 'Annuler'
	},
	deleteSection: {
		title: 'Supprimer la Section',
		description: 'Êtes-vous sûr de vouloir supprimer cette section ?',
		confirm: 'Supprimer',
		cancel: 'Annuler'
	}
};
