export default {
	main: {
		title: 'Mes CVs',
		hello: 'test',
		titleCTA: 'Créer nouveau',
		newCv: 'Nouveau CV',
		newCvDescription: "Créez un CV personnalisé pour chaque offre d'emploi. Augmentez vos chances d'être embauché!"
	},
	profile: {
		plan: {
			upgradePlan: 'Activez votre abonnement',
			title: 'Votre abonnement',
			descriptionPaid:
				'You are on the free plan. You can create and save one CV. Upgrade for PDF download unlimited CV creation.',
			descriptionFree:
				"Vous êtes sur l'abonnement gratuit. Vous pouvez créer et enregistrer un CV. Passez à la version supérieure pour le téléchargement de PDF et la création illimitée de CV."
		},
		title: 'Mon compte',
		dangerZone: 'Zone de danger',
		fields: {
			name: {
				label: 'Prénom'
			},
			lastName: {
				label: 'Nom de famille'
			},
			email: {
				label: 'E-mail'
			}
		},
		purgingExplanation:
			'Une fois que vous avez supprimé votre compte, vous ne pouvez plus annuler. Cela est définitif.',
		emailExplanation:
			"Il s'agit de l'adresse électronique que vous utiliserez pour vous connecter à votre compte et recevoir des notifications.",
		verificationEmailSent: 'Un e-mail de confirmation a été envoyé à {{email}}.'
	}
};
