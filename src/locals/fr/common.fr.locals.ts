export default {
	errors: {
		fileFormat:
			"Le type de fichier choisi n'est pas autorisé! Veuillez envisager de télécharger l'un de ces types de fichiers",
		fileSize: 'La taille du fichier dépasse la taille autorisée!'
	},
	misc: {
		at: ' à',
		At: 'À',
		untitled: 'Sans titre',
		templatesList: 'Modèles: ',
		saved: 'Enregistré',
		saving: 'En cours ...',
		cvLanguage: 'Langue du CV',
		notSpecified: '(Non spécifié)',
		remove: 'Supprimer',
		updatedAt: 'Mis à jour {{dateTime}}',
		copiedToClipboard: "L'URL est copié dans le presse-papiers",
		languages: 'Langues: '
	},
	buttons: {
		deleteProfile: 'Supprimer mon profil',
		goToPaymentCta: "Créez plus qu'un CV!",
		goBack: 'Page précédente',
		dashboard: 'Tableau de bord',
		accountSettings: 'Info comptes',
		faq: 'FAQ',
		logout: 'Déconnexion',
		backToEditor: "Retour à l'éditeur",
		downloadPDF: 'Télécharger (PDF)',
		share: 'Partager',
		save: 'Enregistrer',
		templatesButton: 'Modèles',
		french: 'Français',
		english: 'Anglais',
		arabic: 'Arabe',
		dismiss: 'Ok'
	},
	cvCardBtns: {
		edit: 'Modifier',
		shareLink: 'Partager le lien',
		download: 'Télécharger (PDF)',
		deleteCv: 'Supprimer'
	}
};
