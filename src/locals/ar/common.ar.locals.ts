export default {
	errors: {
		fileFormat: 'نوع الملف المختار غير مسموح به! الرجاء النظر في تحميل أحد أنواع هذه الملفات',
		fileSize: 'حجم الملف يتجاوز الحجم المسموح به!'
	},
	misc: {
		at: 'في ',
		At: '',
		untitled: 'بدون عنوان',
		templatesList: 'نماذج:',
		saved: 'مسجل',
		saving: 'تسجيل...',
		cvLanguage: 'لغة السيرة الذاتية',
		notSpecified: '(غير محدد)',
		remove: 'حذف',
		updatedAt: 'تم التحديث في {{dateTime}}',
		copiedToClipboard: 'يتم نسخ رابط URL إلى الحافظة',
		languages: 'اللغات: '
	},
	buttons: {
		deleteProfile: 'احذف ملفي الشخصي',
		goToPaymentCta: 'كون سير ذاتية بطريقة غير محدودة',
		goBack: 'الصفحة السابقة',
		dashboard: 'لوحة القيادة',
		accountSettings: 'معلومات الحساب',
		faq: ' الأسئلة الأكثر تداولا',
		logout: 'تسجيل خروج',
		backToEditor: 'رجوع إلى المحرر',
		downloadPDF: 'تحميل PDF',
		share: 'شارك',
		save: 'سجل',
		templatesButton: 'النماذج',
		french: 'فرنسية',
		english: 'إنجليزي',
		arabic: 'العربية',
		dismiss: 'حسناً'
	},
	cvCardBtns: {
		edit: 'تعديل',
		shareLink: 'أنشر الرابط',
		download: 'تحميل PDF',
		deleteCv: 'حذف'
	}
};
