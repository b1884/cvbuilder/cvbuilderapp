import dashboard from './dashboard.ar.locals';
import common from './common.ar.locals';
import modals from './modals.ar.locals';
import builder from './builder.ar.locals';

export default {
	dashboard,
	common,
	modals,
	builder
};
