import dashboard from './dashboard.en.locals';
import common from './common.en.locals';
import modals from './modals.en.locals';
import builder from './builder.en.locals';

export default {
	dashboard,
	common,
	modals,
	builder
};
