export default {
	main: {
		title: 'My CVs',
		hello: 'test',
		titleCTA: 'Create a new CV',
		newCv: 'New CV',
		newCvDescription: 'Create a customized resume for each job application. Double your chances of being hired!'
	},
	profile: {
		plan: {
			upgradePlan: 'Upgrade',
			title: 'Your subscription plan',
			descriptionPaid:
				'You are on the free plan. You can create and save one CV. Upgrade for PDF download and unlimited CV creation.',
			descriptionFree:
				'You are on the free plan. You can create and save one CV. Upgrade for PDF download unlimited CV creation.'
		},
		title: 'My account',
		dangerZone: 'Danger zone',
		fields: {
			name: {
				label: 'First name'
			},
			lastName: {
				label: 'Last name'
			},
			email: {
				label: 'E-mail'
			}
		},
		purgingExplanation: 'Once you have deleted your account, you cannot cancel. This is permanent.',
		emailExplanation: 'This is the email address you will use to log into your account and receive notifications.',
		verificationEmailSent: 'A confirmation e-mail was sent to {{email}}.'
	}
};
