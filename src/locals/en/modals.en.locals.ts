export default {
	shareLink: {
		title: 'Share a link to your resume',
		description:
			'Copy and paste the URL to send your resume via SMS, email or to share your resume on your personal website.',
		linkLabel: 'Copy this private URL',
		buttons: {
			openLink: 'Open link',
			copyLink: 'Copy link'
		}
	},
	deleteCv: {
		title: 'Delete CV',
		description: 'Are you sure you want to delete this CV? Once deleted, this CV cannot be restored.',
		confirm: 'Delete',
		cancel: 'Cancel'
	},
	deleteProfile: {
		title: 'Are you sure you want to delete your account?',
		description:
			'Once you click delete, your account and associated data will be permanently deleted and cannot be restored. Alternatively, if you keep your free account, the next time you want to edit or update your resume, you will not have to start from scratch.',
		confirm: 'Delete',
		cancel: 'Keep my account'
	},
	deletePhoto: {
		title: 'Delete photo',
		description: 'Are you sure you want to delete this photo?',
		confirm: 'Delete',
		cancel: 'Cancel'
	},
	deleteSlot: {
		title: 'Delete this entry',
		description: 'Are you sure you want to delete this entry?',
		confirm: 'Delete',
		cancel: 'Cancel'
	},
	deleteSection: {
		title: 'Delete this section',
		description: 'Are you sure you want to delete this section?',
		confirm: 'Delete',
		cancel: 'Cancel'
	}
};
