export default {
	errors: {
		fileFormat: 'The selected file type is not allowed! Please consider uploading one of these file types',
		fileSize: 'The file size exceeds the allowed size!'
	},
	misc: {
		at: ' at',
		At: 'At',
		untitled: 'Untitled',
		templatesList: 'Templates: ',
		saved: 'Saved',
		saving: 'Saving ...',
		cvLanguage: 'CV language',
		notSpecified: '(Not specified)',
		remove: 'Delete',
		updatedAt: 'Updated {{dateTime}}',
		copiedToClipboard: 'The URL is copied to the clipboard',
		languages: 'Languages: '
	},
	buttons: {
		deleteProfile: 'Delete my profile',
		goToPaymentCta: 'Get unlimited CVs!',
		goBack: 'Previous page',
		dashboard: 'Dashboard',
		accountSettings: 'Account Info',
		faq: 'FAQ',
		logout: 'Logout',
		backToEditor: 'Back to editor',
		downloadPDF: 'Download (PDF)',
		share: 'Share',
		save: 'Save',
		templatesButton: 'Templates',
		french: 'French',
		english: 'English',
		arabic: 'Arabic',
		dismiss: 'Okay'
	},
	cvCardBtns: {
		edit: 'Edit',
		shareLink: 'Share link',
		download: 'Download (PDF)',
		deleteCv: 'Delete'
	}
};
