import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardWrapperComponent } from './dashboard-wrapper/dashboard-wrapper.component';
import { MainPageComponent } from './pages/main-page/main-page.component';
import { PaymentPageComponent } from './pages/payment-page/payment-page.component';
import { ProfilePageComponent } from './pages/profile-page/profile-page.component';

const routes: Routes = [
	{
		path: '',
		component: DashboardWrapperComponent,
		children: [
			{
				path: '',
				component: MainPageComponent
			},
			{
				path: 'get-hired-now',
				component: PaymentPageComponent
			},
			{
				path: 'profile',
				component: ProfilePageComponent
			}
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class DashboardRouting {}
