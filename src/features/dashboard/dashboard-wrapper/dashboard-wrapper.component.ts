import { Component, OnInit } from '@angular/core';
import { PubSubService } from '../../../core/services/pub-sub.service';
import { PubSubTopicsEnum } from '../../../core/enums/pub-sub-topics.enum';

@Component({
	selector: 'blackbird-dashboard-wrapper',
	templateUrl: './dashboard-wrapper.component.html',
	styleUrls: ['./dashboard-wrapper.component.scss']
})
export class DashboardWrapperComponent implements OnInit {
	menuState = true;

	constructor(private _pubSubService: PubSubService) {}

	ngOnInit(): void {
		this._pubSubService.sub().subscribe((value) => {
			if (value.topic === PubSubTopicsEnum.toggleMenu) {
				this.menuState = value.data as boolean;
			}
		});
	}
}
