import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../../shared/classes/base-component.class';
import { PubSubTopicsEnum } from '../../../../core/enums/pub-sub-topics.enum';
import { NavigationEnd } from '@angular/router';
import { UserInterface } from '../../../../types/user.type';

@Component({
	selector: 'blackbird-nav-bar',
	templateUrl: './nav-bar.component.html',
	styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent extends BaseComponent implements OnInit {
	open = true;
	activeUrl = '/';
	currentUser!: Promise<UserInterface | null>;

	constructor() {
		super();
	}

	toggleMenu(): void {
		this.open = !this.open;
		this.pubSubService.pub({
			topic: PubSubTopicsEnum.toggleMenu,
			data: this.open
		});
	}

	ngOnInit(): void {
		this.currentUser = this.usersService.currentUserAPI();

		this.pubSubService.sub().subscribe((value) => {
			if (value.topic === PubSubTopicsEnum.toggleMenu) {
				this.open = value.data as boolean;
			}
		});

		this.activeUrl = this.router.url.split('#')[0];

		this.router.events.subscribe((event) => {
			if (event instanceof NavigationEnd) {
				this.activeUrl = event.url.split('#')[0];
			}
		});
	}

	goBack(): void {
		void this.toggleDropdown(true);
		this.location.back();
	}
}
