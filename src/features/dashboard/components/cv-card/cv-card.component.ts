import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BaseComponent } from '../../../../shared/classes/base-component.class';
import { ShareLinkModalComponent } from '../../../../shared/components/share-link-modal/share-link-modal.component';
import { first } from 'rxjs/operators';
import {
	ConfirmationModalComponent,
	ConfirmationModalInterface
} from '../../../../shared/components/confirmation-modal/confirmation-modal.component';
import { ClassicResumePreviewType } from '../../../../types/classic-resume.types';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import localeEnGb from '@angular/common/locales/en-GB';
import localeArSa from '@angular/common/locales/ar-SA';

@Component({
	selector: 'blackbird-cv-card',
	templateUrl: './cv-card.component.html',
	styleUrls: ['./cv-card.component.scss']
})
export class CvCardComponent extends BaseComponent implements OnInit {
	@Output() titleChange: EventEmitter<string> = new EventEmitter<string>();
	@Output() downloadResume: EventEmitter<ClassicResumePreviewType> = new EventEmitter<ClassicResumePreviewType>();
	@Output() deleteResume: EventEmitter<ClassicResumePreviewType> = new EventEmitter<ClassicResumePreviewType>();
	@Input() resume!: ClassicResumePreviewType;

	constructor() {
		super();
	}

	ngOnInit(): void {
		registerLocaleData(localeFr, 'fr');
		registerLocaleData(localeEnGb, 'en');
		registerLocaleData(localeArSa, 'ar');
	}

	showShareLinkDialog(): void {
		this.modalService
			.addModal<Pick<ClassicResumePreviewType, 'sharingLink'>, void>(ShareLinkModalComponent, {
				sharingLink: this.resume.sharingLink
			})
			.pipe(first())
			.subscribe(() => console.log('nice'));
	}

	showConfirmationDialog(): void {
		this.modalService
			.addModal<ConfirmationModalInterface, boolean>(ConfirmationModalComponent, {
				confirmationText: this.t.modals.deleteCv.confirm,
				description: this.t.modals.deleteCv.description,
				cancelText: this.t.modals.deleteCv.cancel,
				title: this.t.modals.deleteCv.title,
				reverseButtons: true
			})
			.pipe(first())
			.subscribe((response: boolean) => {
				if (!response) {
					return;
				}

				this.deleteResume.emit(this.resume);
			});
	}

	handleResumeTitleChange(title: string): void {
		this.titleChange.emit(title);
	}

	downloadResumeAsPdf(): void {
		this.downloadResume.emit(this.resume);
	}

	isSameYear(): boolean {
		if (!this.resume) {
			return false;
		}

		const thisYear = new Date().getFullYear();
		const updateYear = new Date(this.resume.lastUpdateDate).getFullYear();

		return thisYear === updateYear;
	}
}
