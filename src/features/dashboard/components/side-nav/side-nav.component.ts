import { Component, Input, OnInit } from '@angular/core';
import { BaseComponent } from '../../../../shared/classes/base-component.class';
import { PubSubTopicsEnum } from '../../../../core/enums/pub-sub-topics.enum';
import { NavigationEnd, NavigationStart } from '@angular/router';
import { UserInterface } from '../../../../types/user.type';

@Component({
	selector: 'blackbird-side-nav',
	templateUrl: './side-nav.component.html',
	styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent extends BaseComponent implements OnInit {
	@Input() hideMenu = true;
	activeUrl = '/';
	currentUser!: Promise<UserInterface | null>;

	constructor() {
		super();
	}

	toggleMenu(): void {
		this.pubSubService.pub({
			topic: PubSubTopicsEnum.toggleMenu,
			data: !this.hideMenu
		});
	}

	ngOnInit(): void {
		this.currentUser = this.usersService.currentUserAPI();
		this.activeUrl = this.router.url.split('#')[0];

		this.router.events.subscribe((event) => {
			if (event instanceof NavigationStart && !this.hideMenu) {
				this.toggleMenu();
			}

			if (event instanceof NavigationEnd) {
				this.activeUrl = event.url.split('#')[0];
			}
		});
	}
}
