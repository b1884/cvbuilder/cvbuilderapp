import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainPageComponent } from './pages/main-page/main-page.component';
import { PaymentPageComponent } from './pages/payment-page/payment-page.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { CvCardComponent } from './components/cv-card/cv-card.component';
import { DashboardWrapperComponent } from './dashboard-wrapper/dashboard-wrapper.component';
import { DashboardRouting } from './dashboard.routing';
import { SharedModule } from '../../shared/shared.module';
import { SideNavComponent } from './components/side-nav/side-nav.component';
import { ProfilePageComponent } from './pages/profile-page/profile-page.component';

@NgModule({
	declarations: [
		MainPageComponent,
		PaymentPageComponent,
		NavBarComponent,
		CvCardComponent,
		DashboardWrapperComponent,
		SideNavComponent,
		ProfilePageComponent
	],
	imports: [CommonModule, DashboardRouting, SharedModule]
})
export class DashboardModule {}
