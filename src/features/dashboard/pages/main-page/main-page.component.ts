import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../../shared/classes/base-component.class';
import { ClassicResumePreviewType } from '../../../../types/classic-resume.types';
import { saveAs } from 'file-saver';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
	selector: 'blackbird-main-page',
	templateUrl: './main-page.component.html',
	styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent extends BaseComponent implements OnInit {
	classicResumesPreviews: ClassicResumePreviewType[] | null = null;

	constructor() {
		super();
	}

	ngOnInit(): void {
		void this._getResumesList();
	}

	async downloadResume(resume: ClassicResumePreviewType): Promise<void> {
		this.loaderService.showLoader();

		try {
			const pdfLink: string[] | null = await this.builderService.downloadResumeAPI(
				resume.link,
				resume.userId,
				resume._id as string
			);

			if (!pdfLink) {
				return;
			}

			saveAs(pdfLink[1], pdfLink[0]);
			this.loaderService.hideLoader();
		} catch (e) {
			this.loaderService.hideLoader();
			if (!e) {
				return;
			}

			if (e instanceof HttpErrorResponse) {
				alert(e.error.message);
			}
		}
	}

	private async _getResumesList() {
		this.classicResumesPreviews = await this.builderService.getPreviewsListAPI();
	}

	async updateResumeTitle(title: string, resume: ClassicResumePreviewType): Promise<void> {
		await this.builderService.updateResumeAPI({ title, _id: resume._id });
		await this._getResumesList();
	}

	async deleteResume(event: ClassicResumePreviewType): Promise<void> {
		this.loaderService.showLoader();
		await this.builderService.deleteResumeAPI(event);
		await this._getResumesList();
		this.loaderService.hideLoader();
	}

	async createNewResume(): Promise<void> {
		this.loaderService.showLoader();
		const newResumeResp = await this.builderService.createNewAPI();

		if (!newResumeResp) {
			alert("Couldn't create new CV, please check with Dev!");
			return;
		}

		await this.router.navigate([this.routes.builder(newResumeResp.resumeLink).builder]);
		this.loaderService.hideLoader();
	}
}
