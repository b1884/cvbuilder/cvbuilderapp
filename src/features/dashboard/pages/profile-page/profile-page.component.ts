import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../../shared/classes/base-component.class';
import { UserInterface } from '../../../../types/user.type';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { isEqual, omitBy } from 'lodash';
import {
	ConfirmationModalComponent,
	ConfirmationModalInterface
} from '../../../../shared/components/confirmation-modal/confirmation-modal.component';
import { first } from 'rxjs/operators';
import { firstValueFrom } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
	selector: 'blackbird-profile-page',
	templateUrl: './profile-page.component.html',
	styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent extends BaseComponent implements OnInit {
	currentUser!: UserInterface | null;
	isNameFocused = false;
	isLastNameFocused = false;
	isEmailFocused = false;
	profileForm!: UntypedFormGroup;
	sending = false;

	constructor() {
		super();
	}

	ngOnInit(): void {
		void this._initUserAndForm();
	}

	private async _initUserAndForm(): Promise<void> {
		this.currentUser = await this.usersService.currentUserAPI();

		if (!this.currentUser) {
			return;
		}

		this.profileForm = new UntypedFormGroup({
			name: new UntypedFormControl(this.currentUser.name),
			lastName: new UntypedFormControl(this.currentUser.lastName),
			email: new UntypedFormControl(this.currentUser.email, [Validators.email])
		});
	}

	isProfileEdited(): boolean {
		if (!this.currentUser) {
			return false;
		}

		const { email, name, lastName } = this.currentUser;
		return !isEqual(this.profileForm.getRawValue(), { email, name, lastName });
	}

	async submit(): Promise<void> {
		if (!this.currentUser) {
			return;
		}

		this.loaderService.showLoader();
		const payload = omitBy(
			this.profileForm.getRawValue(),
			(value, key: keyof UserInterface) => value === (this.currentUser as UserInterface)[key]
		);

		try {
			await this.usersService.updateUserAPI(payload);
			this.currentUser = await this.usersService.currentUserAPI(true);

			if (payload.email) {
				this.profileForm.get('email')?.setValue(this.currentUser?.email);
			}

			this.loaderService.hideLoader();
		} catch (e) {
			this.loaderService.hideLoader();
			if (!e) {
				return;
			}

			if (e instanceof HttpErrorResponse) {
				alert(e.error.message);
			}
		}
	}

	async showConfirmationDialog(): Promise<void> {
		try {
			const willDelete: boolean = await firstValueFrom<boolean>(
				this.modalService.addModal<ConfirmationModalInterface, boolean>(ConfirmationModalComponent, {
					confirmationText: this.t.modals.deleteProfile.confirm,
					description: this.t.modals.deleteProfile.description,
					cancelText: this.t.modals.deleteProfile.cancel,
					title: this.t.modals.deleteProfile.title,
					reverseButtons: true
				})
			);

			if (!willDelete) {
				return;
			}
			this.loaderService.showLoader();

			await this.usersService.purgeUserAPI();

			location.href = this.routes.facade.home;
			this.loaderService.hideLoader();
		} catch (e) {
			this.loaderService.hideLoader();
			if (!e) {
				return;
			}

			if (e instanceof HttpErrorResponse) {
				alert(e.error.message);
			}
		}
	}
}
