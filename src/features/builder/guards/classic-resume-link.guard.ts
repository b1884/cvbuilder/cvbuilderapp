import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, UrlTree } from '@angular/router';
import { BuilderService } from '../../../core/services/builder.service';
import routes from '../../../routes';
import { ClassicResumeInterface } from '../../../types/classic-resume.types';

@Injectable({
	providedIn: 'root'
})
export class ClassicResumeLinkGuard implements CanActivate {
	constructor(private _builderService: BuilderService, private _router: Router) {}

	async canActivate(route: ActivatedRouteSnapshot): Promise<boolean | UrlTree> {
		try {
			const link: string = route.firstChild?.params.resumeId as string;

			if (!link) {
				await this._router.navigate([routes.dashboard.main]);
				return false;
			}

			const currentResume: ClassicResumeInterface | null = await this._builderService.currentResumeAPI(link);

			if (!currentResume) {
				await this._router.navigate([routes.dashboard.main]);
				return false;
			}
		} catch (e) {
			await this._router.navigate([routes.dashboard.main]);
			return false;
		}
		return true;
	}
}
