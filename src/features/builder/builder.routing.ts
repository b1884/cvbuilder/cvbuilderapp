import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BuilderWrapperComponent } from './builder-wrapper/builder-wrapper.component';
import { BuilderPageComponent } from './pages/builder-page/builder-page.component';
import { TemplatesPageComponent } from './pages/templates-page/templates-page.component';
import { ClassicResumeLinkGuard } from './guards/classic-resume-link.guard';

const routes: Routes = [
	{
		path: '',
		canActivate: [ClassicResumeLinkGuard],
		component: BuilderWrapperComponent,
		children: [
			{
				path: ':resumeId',
				component: BuilderPageComponent
			},
			{
				path: ':resumeId/preview',
				component: TemplatesPageComponent
			}
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class BuilderRouting {}
