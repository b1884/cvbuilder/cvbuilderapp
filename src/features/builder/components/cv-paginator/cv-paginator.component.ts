import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';

@Component({
	selector: 'blackbird-cv-paginator',
	templateUrl: './cv-paginator.component.html',
	styleUrls: ['./cv-paginator.component.scss']
})
export class CvPaginatorComponent implements OnChanges {
	@Input() isTemplatesPage = false;
	@Input() currentPage = 1;
	@Input() totalPages = 1;

	@Output() goTo: EventEmitter<number> = new EventEmitter<number>();

	pagesText = `${this.currentPage} / ${this.totalPages}`;

	triggerPrevPage(): void {
		if (this.currentPage > 1) {
			this.goTo.emit(this.currentPage - 1);
		}
	}

	triggerNextPage(): void {
		if (this.currentPage < this.totalPages) {
			this.goTo.emit(this.currentPage + 1);
		}
	}

	ngOnChanges(changes: SimpleChanges): void {
		if (changes.currentPage || changes.totalPages) {
			this.pagesText = `${this.currentPage} / ${this.totalPages}`;
		}
	}

	isRTL(): boolean {
		return localStorage.getItem('lang') === 'ar';
	}
}
