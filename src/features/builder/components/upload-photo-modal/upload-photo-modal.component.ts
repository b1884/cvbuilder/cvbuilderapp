import { Component } from '@angular/core';

@Component({
	selector: 'blackbird-upload-photo-modal',
	templateUrl: './upload-photo-modal.component.html',
	styleUrls: ['./upload-photo-modal.component.scss']
})
export class UploadPhotoModalComponent {}
