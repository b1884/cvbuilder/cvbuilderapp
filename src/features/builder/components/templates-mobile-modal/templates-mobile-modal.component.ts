import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { SimpleModalComponent } from 'ngx-simple-modal';
import { CarouselComponent, OwlOptions, SlidesOutputData } from 'ngx-owl-carousel-o';
import { TranslationPathsService } from '../../../../core/services/translation-paths.service';
import { PubSubService } from '../../../../core/services/pub-sub.service';
import { PubSubTopicsEnum } from '../../../../core/enums/pub-sub-topics.enum';
import { TemplateInterface } from '../../../../types/template.types';

export interface TemplatesMobileModalInterface {
	templates: TemplateInterface[];
}

@Component({
	selector: 'blackbird-templates-mobile-modal',
	templateUrl: './templates-mobile-modal.component.html',
	styleUrls: ['./templates-mobile-modal.component.scss']
})
export class TemplatesMobileModalComponent
	extends SimpleModalComponent<TemplatesMobileModalInterface, void>
	implements OnInit, OnDestroy, TemplatesMobileModalInterface
{
	@ViewChild('owlCarousel') owlCarousel!: CarouselComponent;

	theModal!: HTMLElement;
	carouselOptions: OwlOptions = {
		dots: false,
		center: true,
		rtl: this.isRTL()
	};
	templates!: TemplateInterface[];

	constructor(private _t: TranslationPathsService, private _pubsubService: PubSubService) {
		super();
	}

	get t(): TranslationPathsService {
		return this._t;
	}

	ngOnInit(): void {
		this.theModal = <HTMLElement>document.getElementsByClassName('modal-content').item(0);
		this.theModal.style.background = 'transparent';

		setTimeout(() => {
			const selectedTemplate = this.templates.filter((template: TemplateInterface) => template.selected)[0];
			this.moveToCarousel(selectedTemplate.codeName);

			this.owlCarousel.changed.subscribe((output: SlidesOutputData) => {
				if (output && output.slides) {
					const selectedSlide = output.slides.filter((slide) => slide.isActive)[0].id;

					if (selectedSlide) {
						this._pubsubService.pub({ topic: PubSubTopicsEnum.changeCvTemplate, data: selectedSlide });
					}
				}
			});
		});
	}

	ngOnDestroy(): void {
		this.theModal.style.background = '#ffffff';
	}

	moveToCarousel(templateCodeName: string): void {
		const selectedTemplate = this.templates.filter((template: TemplateInterface) =>
			template.codeName.includes(templateCodeName)
		);

		if (selectedTemplate && selectedTemplate.length) {
			this.owlCarousel.to(templateCodeName);
			this._pubsubService.pub({ topic: PubSubTopicsEnum.changeCvTemplate, data: templateCodeName });
		}
	}

	isRTL(): boolean {
		return localStorage.getItem('lang') === 'ar';
	}
}
