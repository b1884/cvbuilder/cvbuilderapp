import { Component } from '@angular/core';

@Component({
	selector: 'blackbird-select-template-card',
	templateUrl: './select-template-card.component.html',
	styleUrls: ['./select-template-card.component.scss']
})
export class SelectTemplateCardComponent {}
