import {
	Component,
	ElementRef,
	EventEmitter,
	Input,
	OnChanges,
	OnDestroy,
	Output,
	SimpleChanges,
	ViewChild
} from '@angular/core';
import { ClassicResumeInterface } from '../../../../types/classic-resume.types';
import { DocumentPreviewManagerService } from '../../../../core/services/document-preview-manager.service';
import { BaseComponent } from '../../../../shared/classes/base-component.class';
import { BuilderImageSizeInterface } from '../../../../core/utils/images.util';
import {
	getDocument,
	GlobalWorkerOptions,
	PDFDocumentLoadingTask,
	PDFDocumentProxy,
	version as PDFJSVersion
} from 'pdfjs-dist';

@Component({
	selector: 'blackbird-document-preview',
	templateUrl: './document-preview.component.html',
	styleUrls: ['./document-preview.component.scss']
})
export class DocumentPreviewComponent extends BaseComponent implements OnChanges, OnDestroy {
	@Input() buildPayload!: ClassicResumeInterface;
	@Input() imageSize!: BuilderImageSizeInterface;
	@Input() currentPage = 1;

	@Output() totalPages: EventEmitter<number> = new EventEmitter<number>();
	@Output() isBuilding: EventEmitter<boolean> = new EventEmitter<boolean>();

	@ViewChild('previewCanvas') previewCanvas!: ElementRef<HTMLCanvasElement>;

	pdfContent!: ArrayBuffer | undefined;

	private _pdfDocProxy!: PDFDocumentProxy;
	private _isRenderingPage = false;
	private _pendingPage: number | null = null;

	private _changesTimeout: unknown;

	constructor(private _docPreviewManager: DocumentPreviewManagerService) {
		GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${PDFJSVersion}/pdf.worker.js`;
		super();
	}

	async ngOnChanges(changes: SimpleChanges) {
		if (changes && changes.buildPayload) {
			await this._cycleAsyncWrapper();
		}

		if (changes && (changes.currentPage || changes.imageSize)) {
			this._queueRenderPage();
		}
	}

	private async _initPreview() {
		if (!this.buildPayload) {
			throw 'must pass resume content first!';
		}

		if (!this.pdfContent) {
			throw 'could not get PDF';
		}

		if (!this.previewCanvas || !this.previewCanvas.nativeElement) {
			throw 'no canvas detected';
		}

		await this._loadPdf();

		if (!this._pdfDocProxy) {
			throw 'error pdfDocProxy';
		}

		this._queueRenderPage();
	}

	private _getCanvasContext(): CanvasRenderingContext2D {
		const context = this.previewCanvas.nativeElement.getContext('2d');

		if (!context) {
			console.error('no context');
			throw 'no canvas context detected';
		}

		return context;
	}

	private async _renderPage() {
		this.isBuilding.emit(true);
		if (this._docPreviewManager.shouldBuildAgain(this.buildPayload)) {
			this.pdfContent = await this._buildResume();
			await this._loadPdf();

			if (!this._pdfDocProxy) {
				throw 'error pdfDocProxy';
			}

			await this._renderPage();
		}

		this._isRenderingPage = true;
		const page = await this._pdfDocProxy.getPage(this.currentPage);
		page.cleanup();

		const scale = 1;
		const viewport = page.getViewport({ scale });

		const newScale = this.imageSize.width / viewport.width;
		const scaledViewport = page.getViewport({ scale: newScale });

		try {
			await page.render({
				canvasContext: this._getCanvasContext(),
				viewport: scaledViewport
			}).promise;
		} catch (e) {
			console.error(e);
		}

		this._isRenderingPage = false;
		this.isBuilding.emit(false);

		if (this._pendingPage !== null) {
			await this._renderPage();
			this._pendingPage = null;
		}
	}

	private _queueRenderPage() {
		if (this._changesTimeout) {
			clearTimeout(this._changesTimeout as never);
		}

		this._changesTimeout = setTimeout(() => {
			if (this._isRenderingPage) {
				this._pendingPage = this.currentPage;
			} else {
				void this._renderPage();
			}
		}, 800);
	}

	private async _loadPdf() {
		const pdfLoadingTask: PDFDocumentLoadingTask = getDocument({
			data: this.pdfContent as never
		});

		const pdf: PDFDocumentProxy = await pdfLoadingTask.promise;

		if (!pdf) {
			throw 'error rendering PDF';
		}

		this._pdfDocProxy = pdf;

		// set total pages number
		this.totalPages.emit(pdf.numPages);
	}

	private async _buildResume(): Promise<ArrayBuffer | undefined> {
		if (!this.buildPayload) {
			return undefined;
		}

		if (!this._docPreviewManager.shouldBuildAgain(this.buildPayload)) {
			return this._docPreviewManager.currentResumeJob.pdf;
		}

		const PDFBuffer = await this.builderService.buildPDFBufferAPI(this.buildPayload);

		if (!PDFBuffer) {
			return undefined;
		}

		this._docPreviewManager.currentResumeJob = {
			content: this.buildPayload,
			pdf: PDFBuffer
		};

		return PDFBuffer;
	}

	private async _cycleAsyncWrapper() {
		this.pdfContent = await this._buildResume();

		await this._initPreview();
	}

	private async _pdfCleanup() {
		if (this._pdfDocProxy) {
			await this._pdfDocProxy.destroy();
			await this._pdfDocProxy.cleanup();
		}
	}

	async ngOnDestroy() {
		await this._pdfCleanup();
	}
}
