import { Component, EventEmitter, Input, Output } from '@angular/core';
import { BaseComponent } from '../../../../shared/classes/base-component.class';

@Component({
	selector: 'blackbird-templates-nav-bar',
	templateUrl: './templates-nav-bar.component.html',
	styleUrls: ['./templates-nav-bar.component.scss']
})
export class TemplatesNavBarComponent extends BaseComponent {
	@Input() resumeLink = '';
	@Input() isBuilding = false;

	@Output() shareLink: EventEmitter<void> = new EventEmitter<void>();
	@Output() downloadCv: EventEmitter<void> = new EventEmitter<void>();
	@Output() templatesActionSheet: EventEmitter<void> = new EventEmitter<void>();

	openShareLinkDialog(): void {
		this.shareLink.emit();
	}

	handleDownloadCv(): void {
		this.downloadCv.emit();
	}

	openTemplatesActionSheet(): void {
		this.templatesActionSheet.emit();
	}
}
