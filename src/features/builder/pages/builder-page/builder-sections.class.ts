import { TranslationPathsService } from '../../../../core/services/translation-paths.service';
import { ServiceLocator } from '../../../../shared/classes/service-locator.class';
import {
	BuilderInputInterface,
	BuilderSectionInterface,
	CustomSectionType,
	SectionCodeType,
	SectionSlotInterface
} from './builder-form.modal';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { FieldInterface, ResumeInputInterface, SlotInterface } from '../../../../types/resume-input.types';
import { generateRandomText } from '../../../../core/utils/misc.util';

export class BuilderSections {
	private readonly _t: TranslationPathsService;
	private _personalDetails!: BuilderSectionInterface;
	private _personalResume!: BuilderSectionInterface;

	private _employmentHistory!: BuilderSectionInterface;
	private _educationHistory!: BuilderSectionInterface;
	private _websitesSocialLinks!: BuilderSectionInterface;
	private _skills!: BuilderSectionInterface;

	private _coursesHistory!: BuilderSectionInterface;
	private _trainingsHistory!: BuilderSectionInterface;
	private _languages!: BuilderSectionInterface;
	private _fun!: BuilderSectionInterface;
	private _extraWorkHistory!: BuilderSectionInterface;
	private _customSection!: BuilderSectionInterface;
	private _otherCustomSections!: {
		[key: string]: BuilderSectionInterface;
	};

	constructor(rawData?: ResumeInputInterface) {
		this._t = ServiceLocator.injector.get<TranslationPathsService>(TranslationPathsService);
		this._initSectionsDef(rawData);
	}

	private _initSectionsDef(rawData?: ResumeInputInterface): void {
		this._otherCustomSections = {};
		this.addSkillsSection(rawData);
		this.addWebsitesSocialLinksSection(rawData);
		this.addEmploymentHistorySection(rawData);
		this.addEducationHistorySection(rawData);
		this.addPersonalResumeSection(rawData);
		this.addPersonalInfoSection(rawData);
		this.addCustomSectionSection();
		this.addCoursesSection(rawData);
		this.addTrainingsSection(rawData);
		this.addLanguagesSection(rawData);
		this.addFunSection(rawData);
		this.addExtraWorkHistorySection(rawData);
		this._reconstructOtherCustomSections(rawData);
	}

	private static _setValue(fieldValue: FieldInterface): FieldInterface {
		let actualValue = fieldValue;
		if (typeof fieldValue === 'object' && 'value' in fieldValue && fieldValue.value) {
			actualValue = fieldValue.value as FieldInterface;
		}

		return actualValue;
	}

	static handleFoldCase(section: BuilderSectionInterface, index: number, unfold: boolean): void {
		if (section.slots && section.slots.length) {
			if (unfold) {
				section.slots.forEach((slot, _i: number) => {
					slot.isUnfolded = index === _i;
				});
				return;
			}

			section.slots.forEach((slot) => {
				slot.isUnfolded = false;
			});
		}
		return;
	}

	get personalDetails(): BuilderSectionInterface {
		return this._personalDetails;
	}

	get personalResume(): BuilderSectionInterface {
		return this._personalResume;
	}

	get employmentHistory(): BuilderSectionInterface {
		return this._employmentHistory;
	}

	get educationHistory(): BuilderSectionInterface {
		return this._educationHistory;
	}

	get websitesSocialLinks(): BuilderSectionInterface {
		return this._websitesSocialLinks;
	}

	get skills(): BuilderSectionInterface {
		return this._skills;
	}

	get coursesHistory(): BuilderSectionInterface {
		return this._coursesHistory;
	}

	get trainingsHistory(): BuilderSectionInterface {
		return this._trainingsHistory;
	}

	get languages(): BuilderSectionInterface {
		return this._languages;
	}

	get fun(): BuilderSectionInterface {
		return this._fun;
	}

	get extraWorkHistory(): BuilderSectionInterface {
		return this._extraWorkHistory;
	}

	get otherCustomSections(): { [key: string]: BuilderSectionInterface } {
		return this._otherCustomSections;
	}

	private _reconstructOtherCustomSections(rawData?: ResumeInputInterface): void {
		const translations = this._t.builder.sections.customSection;
		const possibleCustomSections: FieldInterface[] | undefined = rawData?.fields.filter((v: FieldInterface) =>
			v.code.includes('custom-section')
		);

		if (!possibleCustomSections || !possibleCustomSections.length) {
			return;
		}

		for (const cusSection of possibleCustomSections) {
			this._otherCustomSections[cusSection.code] = {
				title: translations.title,
				code: cusSection.code,
				customTitle: cusSection.customTitle,
				formGroup: new UntypedFormGroup({}),
				draggable: true,
				isDeletable: true,
				addSlot: translations.addSlot,
				slots: []
			};

			if (!cusSection.slots || !cusSection.slots.length) {
				continue;
			}

			for (const slot of cusSection.slots) {
				this.addFilledCustomSectionSlot(cusSection.code as CustomSectionType, slot);
			}
		}
	}

	private _getNextCustomSectionIndex(): number {
		const idx: CustomSectionType[] = <CustomSectionType[]>Object.keys(this._otherCustomSections);
		if (idx && idx.length && this._otherCustomSections[idx[idx.length - 1]]) {
			return idx.length;
		}
		return 0;
	}

	generateNewCustomSection(): CustomSectionType {
		const translations = this._t.builder.sections.customSection;
		const customSectionCode: CustomSectionType = `custom-section-${this._getNextCustomSectionIndex()}`;
		this._otherCustomSections[customSectionCode] = {
			title: translations.title,
			code: customSectionCode,
			formGroup: new UntypedFormGroup({}),
			draggable: true,
			isDeletable: true,
			addSlot: translations.addSlot,
			slots: []
		};

		return customSectionCode;
	}

	handleSlotsFormsAddition(code: SectionCodeType, index: number): void {
		switch (code) {
			case 'courses-history':
				return this.addSlotToSectionForm(this._coursesHistory, index);
			case 'trainings-history':
				return this.addSlotToSectionForm(this._trainingsHistory, index);
			case 'languages':
				return this.addSlotToSectionForm(this._languages, index);
			case 'extra-work-history':
				return this.addSlotToSectionForm(this._extraWorkHistory, index);
			case 'skills':
				return this.addSlotToSectionForm(this._skills, index);
			case 'websites-social-links':
				return this.addSlotToSectionForm(this._websitesSocialLinks, index);
			case 'employment-history':
				return this.addSlotToSectionForm(this._employmentHistory, index);
			case 'education-history':
				return this.addSlotToSectionForm(this._educationHistory, index);
			default:
				if (code.includes('custom-section') && this._otherCustomSections[code]) {
					this.addSlotToSectionForm(this._otherCustomSections[code], index);
				}
				return;
		}
	}

	handleSlotFolding(code: SectionCodeType, index: number, unfold: boolean): void {
		switch (code) {
			case 'courses-history':
				return BuilderSections.handleFoldCase(this._coursesHistory, index, unfold);
			case 'trainings-history':
				return BuilderSections.handleFoldCase(this._trainingsHistory, index, unfold);
			case 'languages':
				return BuilderSections.handleFoldCase(this._languages, index, unfold);
			case 'extra-work-history':
				return BuilderSections.handleFoldCase(this._extraWorkHistory, index, unfold);
			case 'skills':
				return BuilderSections.handleFoldCase(this._skills, index, unfold);
			case 'websites-social-links':
				return BuilderSections.handleFoldCase(this._websitesSocialLinks, index, unfold);
			case 'employment-history':
				return BuilderSections.handleFoldCase(this._employmentHistory, index, unfold);
			case 'education-history':
				return BuilderSections.handleFoldCase(this._educationHistory, index, unfold);
			default:
				if (code.includes('custom-section') && this._otherCustomSections[code]) {
					BuilderSections.handleFoldCase(this._otherCustomSections[code], index, unfold);
				}
				return;
		}
	}

	handleSlotDeletion(code: SectionCodeType, index: number): void {
		switch (code) {
			case 'courses-history':
				this._coursesHistory.slots?.splice(index, 1);
				this.removeSlotFromSectionForm(this._coursesHistory, index);
				return;
			case 'trainings-history':
				this._trainingsHistory.slots?.splice(index, 1);
				this.removeSlotFromSectionForm(this._trainingsHistory, index);
				return;
			case 'languages':
				this._languages.slots?.splice(index, 1);
				this.removeSlotFromSectionForm(this._languages, index);
				return;
			case 'extra-work-history':
				this._extraWorkHistory.slots?.splice(index, 1);
				this.removeSlotFromSectionForm(this._extraWorkHistory, index);
				return;
			case 'skills':
				this._skills.slots?.splice(index, 1);
				this.removeSlotFromSectionForm(this._skills, index);
				return;
			case 'websites-social-links':
				this._websitesSocialLinks.slots?.splice(index, 1);
				this.removeSlotFromSectionForm(this._websitesSocialLinks, index);
				return;
			case 'employment-history':
				this._employmentHistory.slots?.splice(index, 1);
				this.removeSlotFromSectionForm(this._employmentHistory, index);
				return;
			case 'education-history':
				this._educationHistory.slots?.splice(index, 1);
				this.removeSlotFromSectionForm(this._educationHistory, index);
				return;
			default:
				if (code.includes('custom-section') && this._otherCustomSections[code]) {
					this._otherCustomSections[code].slots?.splice(index, 1);
					this.removeSlotFromSectionForm(this._otherCustomSections[code], index);
				}
				return;
		}
	}

	/*
	 * adding sections methods
	 * */

	addCoursesSection(rawData?: ResumeInputInterface): void {
		const translations = this._t.builder.sections.coursesHistory;
		this._coursesHistory = {
			title: translations.title,
			code: 'courses-history',
			draggable: true,
			isDeletable: true,
			formGroup: new UntypedFormGroup({}),
			addSlot: translations.addSlot,
			slots: []
		};

		const oldValuesIndex: number | undefined = rawData?.fields.findIndex((v) => v.code === 'courses-history');
		const hasOldValue = oldValuesIndex !== undefined && oldValuesIndex >= 0;
		const oldValues: FieldInterface | null = hasOldValue
			? (rawData?.fields[oldValuesIndex] as FieldInterface)
			: null;

		if (!oldValues || !oldValues.slots) {
			return;
		}

		this._coursesHistory.customTitle = oldValues.customTitle;

		for (const slot of oldValues.slots) {
			this.addFilledCoursesHistorySlot(slot);
		}
	}

	addTrainingsSection(rawData?: ResumeInputInterface): void {
		const translations = this._t.builder.sections.trainingsHistory;
		this._trainingsHistory = {
			title: translations.title,
			code: 'trainings-history',
			draggable: true,
			formGroup: new UntypedFormGroup({}),
			isDeletable: true,
			addSlot: translations.addSlot,
			slots: []
		};

		const oldValuesIndex: number | undefined = rawData?.fields.findIndex((v) => v.code === 'trainings-history');
		const hasOldValue = oldValuesIndex !== undefined && oldValuesIndex >= 0;
		const oldValues: FieldInterface | null = hasOldValue
			? (rawData?.fields[oldValuesIndex] as FieldInterface)
			: null;

		if (!oldValues || !oldValues.slots) {
			return;
		}

		this._trainingsHistory.customTitle = oldValues.customTitle;

		for (const slot of oldValues.slots) {
			this.addFilledTrainingsHistorySlot(slot);
		}
	}

	addLanguagesSection(rawData?: ResumeInputInterface): void {
		const translations = this._t.builder.sections.languages;
		this._languages = {
			title: translations.title,
			code: 'languages',
			formGroup: new UntypedFormGroup({}),
			draggable: true,
			isDeletable: true,
			addSlot: translations.addSlot,
			slots: []
		};

		const oldValuesIndex: number | undefined = rawData?.fields.findIndex((v) => v.code === 'languages');
		const hasOldValue = oldValuesIndex !== undefined && oldValuesIndex >= 0;
		const oldValues: FieldInterface | null = hasOldValue
			? (rawData?.fields[oldValuesIndex] as FieldInterface)
			: null;

		if (!oldValues || !oldValues.slots) {
			return;
		}

		this._languages.customTitle = oldValues.customTitle;

		for (const slot of oldValues.slots) {
			this.addFilledLanguagesSlot(slot);
		}
	}

	addFunSection(rawData?: ResumeInputInterface): void {
		const oldValuesIndex: number | undefined = rawData?.fields.findIndex((v) => v.code === 'fun');
		const hasOldValue = oldValuesIndex !== undefined && oldValuesIndex >= 0;
		const oldValues: FieldInterface | null = hasOldValue
			? (rawData?.fields[oldValuesIndex] as FieldInterface)
			: null;
		const translations = this._t.builder.sections.fun;
		this._fun = {
			title: translations.title,
			customTitle: oldValues?.customTitle,
			code: 'fun',
			draggable: true,
			formGroup: new UntypedFormGroup({}),
			isDeletable: true,
			description: translations.sectionDescription,
			fields: [
				{
					label: '',
					type: 'textarea',
					cssClasses: 'w-full',
					name: 'funActivities',
					id: 'funActivities',
					placeholder: translations.placeholder
				}
			]
		};

		this._fun.fields?.forEach((field: BuilderInputInterface) => {
			if (field.name) {
				const fieldValue =
					oldValues && oldValues[field.name]
						? BuilderSections._setValue(oldValues[field.name] as FieldInterface)
						: null;
				this._fun.formGroup?.addControl(field.name, new UntypedFormControl(fieldValue || ''));
			}
		});
	}

	addExtraWorkHistorySection(rawData?: ResumeInputInterface): void {
		const translations = this._t.builder.sections.extraWorkHistory;
		this._extraWorkHistory = {
			title: translations.title,
			code: 'extra-work-history',
			isDeletable: true,
			formGroup: new UntypedFormGroup({}),
			draggable: true,
			addSlot: translations.addSlot,
			slots: []
		};

		const oldValuesIndex: number | undefined = rawData?.fields.findIndex((v) => v.code === 'extra-work-history');
		const hasOldValue = oldValuesIndex !== undefined && oldValuesIndex >= 0;
		const oldValues: FieldInterface | null = hasOldValue
			? (rawData?.fields[oldValuesIndex] as FieldInterface)
			: null;

		if (!oldValues || !oldValues.slots) {
			return;
		}

		this._extraWorkHistory.customTitle = oldValues.customTitle;

		for (const slot of oldValues.slots) {
			this.addFilledExtraWorkHistorySlot(slot);
		}
	}

	addCustomSectionSection(): void {
		const translations = this._t.builder.sections.customSection;
		this._customSection = {
			title: translations.title,
			code: 'custom-section',
			formGroup: new UntypedFormGroup({}),
			draggable: true,
			isDeletable: true,
			addSlot: translations.addSlot,
			slots: []
		};
	}

	addSkillsSection(rawData?: ResumeInputInterface): void {
		const translations = this._t.builder.sections.skills;
		this._skills = {
			title: translations.title,
			code: 'skills',
			formGroup: new UntypedFormGroup({}),
			draggable: true,
			description: translations.sectionDescription,
			addSlot: translations.addSlot,
			slots: []
		};

		const oldValuesIndex: number | undefined = rawData?.fields.findIndex((v) => v.code === 'skills');
		const hasOldValue = oldValuesIndex !== undefined && oldValuesIndex >= 0;
		const oldValues: FieldInterface | null = hasOldValue
			? (rawData?.fields[oldValuesIndex] as FieldInterface)
			: null;

		if (!oldValues || !oldValues.slots) {
			return;
		}

		this._skills.customTitle = oldValues.customTitle;

		for (const slot of oldValues.slots) {
			this.addFilledSkillsSlot(slot);
		}
	}

	addWebsitesSocialLinksSection(rawData?: ResumeInputInterface): void {
		const translations = this._t.builder.sections.websitesSocialLinks;
		this._websitesSocialLinks = {
			title: translations.title,
			code: 'websites-social-links',
			draggable: true,
			formGroup: new UntypedFormGroup({}),
			description: translations.sectionDescription,
			addSlot: translations.addSlot,
			slots: []
		};

		const oldValuesIndex: number | undefined = rawData?.fields.findIndex((v) => v.code === 'websites-social-links');
		const hasOldValue = oldValuesIndex !== undefined && oldValuesIndex >= 0;
		const oldValues: FieldInterface | null = hasOldValue
			? (rawData?.fields[oldValuesIndex] as FieldInterface)
			: null;

		if (!oldValues || !oldValues.slots) {
			return;
		}

		this._websitesSocialLinks.customTitle = oldValues.customTitle;

		for (const slot of oldValues.slots) {
			this.addFilledWebsitesSlot(slot);
		}
	}

	addEducationHistorySection(rawData?: ResumeInputInterface): void {
		const translations = this._t.builder.sections.educationHistory;
		this._educationHistory = {
			title: translations.title,
			code: 'education-history',
			formGroup: new UntypedFormGroup({}),
			draggable: true,
			description: translations.sectionDescription,
			addSlot: translations.addSlot,
			slots: []
		};

		const oldValuesIndex: number | undefined = rawData?.fields.findIndex((v) => v.code === 'education-history');
		const hasOldValue = oldValuesIndex !== undefined && oldValuesIndex >= 0;
		const oldValues: FieldInterface | null = hasOldValue
			? (rawData?.fields[oldValuesIndex] as FieldInterface)
			: null;

		if (!oldValues || !oldValues.slots) {
			return;
		}

		this._educationHistory.customTitle = oldValues.customTitle;

		for (const slot of oldValues.slots) {
			this.addFilledEducationSlot(slot);
		}
	}

	addEmploymentHistorySection(rawData?: ResumeInputInterface): void {
		const translations = this._t.builder.sections.employmentHistory;
		this._employmentHistory = {
			title: translations.title,
			code: 'employment-history',
			formGroup: new UntypedFormGroup({}),
			draggable: true,
			description: translations.sectionDescription,
			addSlot: translations.addSlot,
			slots: []
		};

		const oldValuesIndex: number | undefined = rawData?.fields.findIndex((v) => v.code === 'employment-history');
		const hasOldValue = oldValuesIndex !== undefined && oldValuesIndex >= 0;
		const oldValues: FieldInterface | null = hasOldValue
			? (rawData?.fields[oldValuesIndex] as FieldInterface)
			: null;

		if (!oldValues || !oldValues.slots) {
			return;
		}

		this._employmentHistory.customTitle = oldValues.customTitle;

		for (const slot of oldValues.slots) {
			this.addFilledEmploymentSlot(slot);
		}
	}

	addPersonalInfoSection(rawData?: ResumeInputInterface): void {
		const oldValuesIndex: number | undefined = rawData?.fields.findIndex((v) => v.code === 'personal-details');
		const hasOldValue = oldValuesIndex !== undefined && oldValuesIndex >= 0;
		const oldValues: FieldInterface | null = hasOldValue
			? (rawData?.fields[oldValuesIndex] as FieldInterface)
			: null;
		const translations = this._t.builder.sections.personalInfo;
		this._personalDetails = {
			title: translations.title,
			customTitle: oldValues?.customTitle,
			isFoldable: true,
			formGroup: new UntypedFormGroup({}),
			code: 'personal-details',
			fields: [
				{
					type: 'profile-picture',
					label: '',
					name: '',
					placeholder: '',
					cssClasses: 'w-full'
				},
				{
					type: 'text',
					name: 'desiredPostTitle',
					cssClasses: 'w-full',
					id: 'desiredPostTitle',
					hint: translations.postTitle.hint,
					label: translations.postTitle.label,
					placeholder: translations.postTitle.placeholder
				},
				{
					type: 'text',
					name: 'firstName',
					cssClasses: '',
					placeholder: '',
					id: 'firstName',
					label: translations.firstName.label
				},
				{
					type: 'text',
					name: 'lastName',
					cssClasses: '',
					placeholder: '',
					id: 'lastName',
					label: translations.lastName.label
				},
				{
					type: 'text',
					name: 'email',
					cssClasses: '',
					placeholder: '',
					id: 'email',
					label: translations.email.label
				},
				{
					type: 'text',
					name: 'phone',
					cssClasses: '',
					placeholder: '',
					id: 'phone',
					label: translations.phone.label
				},
				{
					type: 'text',
					name: 'country',
					isFoldable: true,
					cssClasses: '',
					placeholder: '',
					id: 'country',
					label: translations.country.label
				},
				{
					type: 'text',
					name: 'city',
					isFoldable: true,
					cssClasses: '',
					placeholder: '',
					id: 'city',
					label: translations.city.label
				},
				{
					type: 'text',
					name: 'address',
					isFoldable: true,
					cssClasses: '',
					id: 'address',
					placeholder: '',
					label: translations.address.label
				},
				{
					type: 'text',
					name: 'zipCode',
					isFoldable: true,
					cssClasses: '',
					id: 'zipCode',
					placeholder: '',
					label: translations.zipCode.label
				},
				{
					type: 'text',
					name: 'drivingLicense',
					isFoldable: true,
					cssClasses: '',
					id: 'drivingLicense',
					placeholder: '',
					label: translations.drivingLicense.label,
					hint: translations.drivingLicense.hint
				},
				{
					type: 'text',
					name: 'nationality',
					isFoldable: true,
					cssClasses: '',
					id: 'nationality',
					placeholder: '',
					label: translations.nationality.label,
					hint: translations.nationality.hint
				},
				{
					type: 'text',
					name: 'birthLocation',
					isFoldable: true,
					cssClasses: '',
					id: 'birthLocation',
					placeholder: '',
					label: translations.birthLocation.label
				},
				{
					type: 'date',
					name: 'birthDate',
					isFoldable: true,
					cssClasses: '',
					id: 'birthDate',
					placeholder: '',
					label: translations.birthDate.label,
					hint: translations.birthDate.hint
				}
			]
		};

		this._personalDetails.fields?.forEach((field: BuilderInputInterface) => {
			if (field.name) {
				const fieldValue =
					oldValues && oldValues[field.name]
						? BuilderSections._setValue(oldValues[field.name] as FieldInterface)
						: null;

				this._personalDetails.formGroup?.addControl(field.name, new UntypedFormControl(fieldValue || ''));
			}
		});
	}

	addPersonalResumeSection(rawData?: ResumeInputInterface): void {
		const oldValuesIndex: number | undefined = rawData?.fields.findIndex((v) => v.code === 'personal-resume');
		const hasOldValue = oldValuesIndex !== undefined && oldValuesIndex >= 0;
		const oldValues: FieldInterface | null = hasOldValue
			? (rawData?.fields[oldValuesIndex] as FieldInterface)
			: null;

		const translations = this._t.builder.sections.personalResume;
		this._personalResume = {
			title: translations.title,
			code: 'personal-resume',
			customTitle: oldValues?.customTitle,
			formGroup: new UntypedFormGroup({}),
			description: translations.sectionDescription,
			fields: [
				{
					label: '',
					type: 'WYSIWYG',
					cssClasses: 'w-full',
					name: 'personalResume',
					id: 'personalResume',
					placeholder: translations.placeholder
				}
			]
		};

		this._personalResume.fields?.forEach((field: BuilderInputInterface) => {
			if (field.name) {
				const fieldValue =
					oldValues && oldValues[field.name]
						? BuilderSections._setValue(oldValues[field.name] as FieldInterface)
						: null;

				this._personalResume.formGroup?.addControl(field.name, new UntypedFormControl(fieldValue || ''));
			}
		});
	}

	/*
	 * adding filled slots methods
	 * */

	addFilledEmploymentSlot(slotData: SlotInterface): void {
		const translations = this._t.builder.sections.employmentHistory;
		const fields: BuilderInputInterface[] = [
			{
				type: 'text',
				name: 'postTitle',
				id: 'postTitle',
				label: translations.postTitle.label,
				placeholder: ''
			},
			{
				type: 'text',
				name: 'host',
				id: 'host',
				label: translations.host.label,
				placeholder: ''
			},
			{
				type: 'month',
				name: 'duration',
				id: 'duration',
				label: translations.duration.label,
				hint: translations.duration.hint,
				placeholder: ''
			},
			{
				type: 'text',
				name: 'city',
				id: 'city',
				label: translations.city.label,
				placeholder: ''
			},
			{
				type: 'WYSIWYG',
				name: 'description',
				id: 'description',
				cssClasses: 'w-full',
				label: translations.description.label,
				placeholder: translations.description.placeholder,
				tip: translations.description.tip
			}
		];
		const slot: SectionSlotInterface = {
			isUnfolded: false,
			formGroup: new UntypedFormGroup({}),
			fields
		};

		slot.fields?.forEach((field: BuilderInputInterface) => {
			if (field.name) {
				slot.formGroup?.addControl(
					field.name,
					new UntypedFormControl(slotData[field.name as keyof SlotInterface] || '')
				);
			}

			if (field.name === 'duration') {
				const key: keyof SlotInterface = `${field.name}End`;
				slot.formGroup?.addControl(key, new UntypedFormControl(slotData[key] || ''));
			}
		});

		const index = this._employmentHistory.slots?.push(slot) as number;

		this.handleSlotsFormsAddition('employment-history', index - 1);
	}

	addFilledEducationSlot(slotData: SlotInterface): void {
		const translations = this._t.builder.sections.educationHistory;
		const fields: BuilderInputInterface[] = [
			{
				type: 'text',
				name: 'postTitle',
				id: 'postTitle',
				label: translations.postTitle.label,
				placeholder: ''
			},
			{
				type: 'text',
				name: 'host',
				id: 'host',
				label: translations.host.label,
				placeholder: ''
			},
			{
				type: 'month',
				name: 'duration',
				id: 'duration',
				label: translations.duration.label,
				hint: translations.duration.hint,
				placeholder: ''
			},
			{
				type: 'text',
				name: 'city',
				id: 'city',
				label: translations.city.label,
				placeholder: ''
			},
			{
				type: 'WYSIWYG',
				name: 'description',
				id: 'description',
				cssClasses: 'w-full',
				label: translations.description.label,
				placeholder: translations.description.placeholder
			}
		];
		const slot: SectionSlotInterface = {
			isUnfolded: false,
			formGroup: new UntypedFormGroup({}),
			fields
		};

		slot.fields?.forEach((field: BuilderInputInterface) => {
			if (field.name) {
				slot.formGroup?.addControl(
					field.name,
					new UntypedFormControl(slotData[field.name as keyof SlotInterface] || '')
				);
			}

			if (field.name === 'duration') {
				const key: keyof SlotInterface = `${field.name}End`;
				slot.formGroup?.addControl(key, new UntypedFormControl(slotData[key] || ''));
			}
		});

		const index = this._educationHistory.slots?.push(slot) as number;

		this.handleSlotsFormsAddition('education-history', index - 1);
	}

	addFilledWebsitesSlot(slotData: SlotInterface): void {
		const translations = this._t.builder.sections.websitesSocialLinks;
		const fields: BuilderInputInterface[] = [
			{
				type: 'text',
				name: 'name',
				id: 'name',
				label: translations.name.label,
				placeholder: ''
			},
			{
				type: 'text',
				name: 'link',
				id: 'link',
				label: translations.link.label,
				placeholder: ''
			}
		];
		const slot: SectionSlotInterface = {
			isUnfolded: false,
			formGroup: new UntypedFormGroup({}),
			fields
		};

		slot.fields?.forEach((field: BuilderInputInterface) => {
			if (field.name) {
				slot.formGroup?.addControl(
					field.name,
					new UntypedFormControl(slotData[field.name as keyof SlotInterface] || '')
				);
			}

			if (field.name === 'duration') {
				const key: keyof SlotInterface = `${field.name}End`;
				slot.formGroup?.addControl(key, new UntypedFormControl(slotData[key] || ''));
			}
		});

		const index = this._websitesSocialLinks.slots?.push(slot) as number;

		this.handleSlotsFormsAddition('websites-social-links', index - 1);
	}

	addFilledSkillsSlot(slotData: SlotInterface): void {
		const translations = this._t.builder.sections.skills;
		const fields: BuilderInputInterface[] = [
			{
				type: 'text',
				name: 'skill',
				id: 'skill',
				label: translations.skill.label,
				placeholder: ''
			},
			{
				type: 'range',
				name: 'level',
				max: 4,
				min: 1,
				value: 3,
				step: 1,
				levelText: translations.level['4'],
				id: 'level',
				label: translations.level.label,
				placeholder: ''
			}
		];
		const slot: SectionSlotInterface = {
			isUnfolded: false,
			formGroup: new UntypedFormGroup({}),
			fields
		};

		slot.fields?.forEach((field: BuilderInputInterface) => {
			if (field.name) {
				slot.formGroup?.addControl(
					field.name,
					new UntypedFormControl(slotData[field.name as keyof SlotInterface] || '')
				);
			}

			if (field.name === 'duration') {
				const key: keyof SlotInterface = `${field.name}End`;
				slot.formGroup?.addControl(key, new UntypedFormControl(slotData[key] || ''));
			}
		});

		const index = this._skills.slots?.push(slot) as number;

		this.handleSlotsFormsAddition('skills', index - 1);
	}

	addFilledLanguagesSlot(slotData: SlotInterface): void {
		const translations = this._t.builder.sections.languages;
		const fields: BuilderInputInterface[] = [
			{
				type: 'text',
				name: 'lang',
				id: 'lang',
				label: translations.lang.label,
				placeholder: ''
			},
			{
				type: 'range',
				name: 'level',
				max: 4,
				min: 1,
				value: 3,
				step: 1,
				levelText: translations.level['3'],
				id: 'level',
				label: translations.level.label,
				placeholder: ''
			}
		];
		const slot: SectionSlotInterface = {
			isUnfolded: false,
			formGroup: new UntypedFormGroup({}),
			fields
		};

		slot.fields?.forEach((field: BuilderInputInterface) => {
			if (field.name) {
				slot.formGroup?.addControl(
					field.name,
					new UntypedFormControl(slotData[field.name as keyof SlotInterface] || '')
				);
			}

			if (field.name === 'duration') {
				const key: keyof SlotInterface = `${field.name}End`;
				slot.formGroup?.addControl(key, new UntypedFormControl(slotData[key] || ''));
			}
		});

		const index = this._languages.slots?.push(slot) as number;

		this.handleSlotsFormsAddition('languages', index - 1);
	}

	addFilledCoursesHistorySlot(slotData: SlotInterface): void {
		const translations = this._t.builder.sections.coursesHistory;
		const fields: BuilderInputInterface[] = [
			{
				type: 'text',
				name: 'postTitle',
				id: 'postTitle',
				label: translations.postTitle.label,
				placeholder: ''
			},
			{
				type: 'text',
				name: 'host',
				id: 'host',
				label: translations.host.label,
				placeholder: ''
			},
			{
				type: 'month',
				name: 'duration',
				id: 'duration',
				label: translations.duration.label,
				hint: translations.duration.hint,
				placeholder: ''
			}
		];
		const slot: SectionSlotInterface = {
			isUnfolded: false,
			formGroup: new UntypedFormGroup({}),
			fields
		};

		slot.fields?.forEach((field: BuilderInputInterface) => {
			if (field.name) {
				slot.formGroup?.addControl(
					field.name,
					new UntypedFormControl(slotData[field.name as keyof SlotInterface] || '')
				);
			}

			if (field.name === 'duration') {
				const key: keyof SlotInterface = `${field.name}End`;
				slot.formGroup?.addControl(key, new UntypedFormControl(slotData[key] || ''));
			}
		});

		const index = this._coursesHistory.slots?.push(slot) as number;

		this.handleSlotsFormsAddition('courses-history', index - 1);
	}

	addFilledTrainingsHistorySlot(slotData: SlotInterface): void {
		const translations = this._t.builder.sections.trainingsHistory;
		const fields: BuilderInputInterface[] = [
			{
				type: 'text',
				name: 'postTitle',
				id: 'postTitle',
				label: translations.postTitle.label,
				placeholder: ''
			},
			{
				type: 'text',
				name: 'host',
				id: 'host',
				label: translations.host.label,
				placeholder: ''
			},
			{
				type: 'month',
				name: 'duration',
				id: 'duration',
				label: translations.duration.label,
				hint: translations.duration.hint,
				placeholder: ''
			},
			{
				type: 'WYSIWYG',
				name: 'description',
				id: 'description',
				cssClasses: 'w-full',
				label: translations.description.label,
				placeholder: translations.description.placeholder
			}
		];
		const slot: SectionSlotInterface = {
			isUnfolded: false,
			formGroup: new UntypedFormGroup({}),
			fields
		};

		slot.fields?.forEach((field: BuilderInputInterface) => {
			if (field.name) {
				slot.formGroup?.addControl(
					field.name,
					new UntypedFormControl(slotData[field.name as keyof SlotInterface] || '')
				);
			}

			if (field.name === 'duration') {
				const key: keyof SlotInterface = `${field.name}End`;
				slot.formGroup?.addControl(key, new UntypedFormControl(slotData[key] || ''));
			}
		});

		const index = this._trainingsHistory.slots?.push(slot) as number;

		this.handleSlotsFormsAddition('trainings-history', index - 1);
	}

	addFilledExtraWorkHistorySlot(slotData: SlotInterface): void {
		const translations = this._t.builder.sections.extraWorkHistory;
		const fields: BuilderInputInterface[] = [
			{
				type: 'text',
				name: 'postTitle',
				id: 'postTitle',
				label: translations.postTitle.label,
				placeholder: ''
			},
			{
				type: 'text',
				name: 'host',
				id: 'host',
				label: translations.host.label,
				placeholder: ''
			},
			{
				type: 'month',
				name: 'duration',
				id: 'duration',
				label: translations.duration.label,
				hint: translations.duration.hint,
				placeholder: ''
			},
			{
				type: 'text',
				name: 'city',
				id: 'city',
				label: translations.city.label,
				placeholder: ''
			},
			{
				type: 'WYSIWYG',
				name: 'description',
				id: 'description',
				cssClasses: 'w-full',
				label: translations.description.label,
				placeholder: ''
			}
		];
		const slot: SectionSlotInterface = {
			isUnfolded: false,
			formGroup: new UntypedFormGroup({}),
			fields
		};

		slot.fields?.forEach((field: BuilderInputInterface) => {
			if (field.name) {
				slot.formGroup?.addControl(
					field.name,
					new UntypedFormControl(slotData[field.name as keyof SlotInterface] || '')
				);
			}

			if (field.name === 'duration') {
				const key: keyof SlotInterface = `${field.name}End`;
				slot.formGroup?.addControl(key, new UntypedFormControl(slotData[key] || ''));
			}
		});

		const index = this._extraWorkHistory.slots?.push(slot) as number;

		this.handleSlotsFormsAddition('extra-work-history', index - 1);
	}

	addFilledCustomSectionSlot(code: CustomSectionType, slotData: SlotInterface): void {
		const translations = this._t.builder.sections.customSection;
		const fields: BuilderInputInterface[] = [
			{
				type: 'text',
				name: 'postTitle',
				id: 'postTitle',
				label: translations.postTitle.label,
				placeholder: ''
			},
			{
				type: 'text',
				name: 'city',
				id: 'city',
				label: translations.city.label,
				placeholder: ''
			},
			{
				type: 'month',
				name: 'duration',
				id: 'duration',
				label: translations.duration.label,
				hint: translations.duration.hint,
				placeholder: ''
			},
			{
				type: 'WYSIWYG',
				name: 'description',
				id: 'description',
				cssClasses: 'w-full',
				label: translations.description.label,
				placeholder: ''
			}
		];
		const slot: SectionSlotInterface = {
			isUnfolded: false,
			formGroup: new UntypedFormGroup({}),
			fields
		};

		slot.fields?.forEach((field: BuilderInputInterface) => {
			if (field.name) {
				slot.formGroup?.addControl(
					field.name,
					new UntypedFormControl(slotData[field.name as keyof SlotInterface] || '')
				);
			}

			if (field.name === 'duration') {
				const key: keyof SlotInterface = `${field.name}End`;
				slot.formGroup?.addControl(key, new UntypedFormControl(slotData[key] || ''));
			}
		});

		const index = (<SectionSlotInterface[]>this._otherCustomSections[code].slots).push(slot);

		this.handleSlotsFormsAddition(code, index - 1);
	}

	/*
	 * adding empty slots methods
	 * */

	addEmploymentSlot(): number {
		const translations = this._t.builder.sections.employmentHistory;
		const fields: BuilderInputInterface[] = [
			{
				type: 'text',
				name: 'postTitle',
				id: 'postTitle',
				label: translations.postTitle.label,
				placeholder: ''
			},
			{
				type: 'text',
				name: 'host',
				id: 'host',
				label: translations.host.label,
				placeholder: ''
			},
			{
				type: 'month',
				name: 'duration',
				id: 'duration',
				label: translations.duration.label,
				hint: translations.duration.hint,
				placeholder: ''
			},
			{
				type: 'text',
				name: 'city',
				id: 'city',
				label: translations.city.label,
				placeholder: ''
			},
			{
				type: 'WYSIWYG',
				name: 'description',
				id: 'description',
				cssClasses: 'w-full',
				label: translations.description.label,
				placeholder: translations.description.placeholder,
				tip: translations.description.tip
			}
		];
		const slot: SectionSlotInterface = {
			isUnfolded: false,
			formGroup: new UntypedFormGroup({}),
			fields
		};

		slot.fields?.forEach((field: BuilderInputInterface) => {
			if (field.name) {
				slot.formGroup?.addControl(field.name, new UntypedFormControl(''));
			}

			if (field.name === 'duration') {
				slot.formGroup?.addControl(`${field.name}End`, new UntypedFormControl(''));
			}
		});

		this.handleSlotFolding(this._employmentHistory.code, 0, false);

		if (this._employmentHistory.slots) {
			slot.formGroup?.addControl('index', new UntypedFormControl(this._employmentHistory.slots.length));
			return this._employmentHistory.slots.push(slot) - 1;
		}

		slot.formGroup?.addControl('index', new UntypedFormControl(0));

		this._employmentHistory.slots = [slot];
		return 0;
	}

	addEducationSlot(): number {
		const translations = this._t.builder.sections.educationHistory;
		const fields: BuilderInputInterface[] = [
			{
				type: 'text',
				name: 'postTitle',
				id: 'postTitle',
				label: translations.postTitle.label,
				placeholder: ''
			},
			{
				type: 'text',
				name: 'host',
				id: 'host',
				label: translations.host.label,
				placeholder: ''
			},
			{
				type: 'month',
				name: 'duration',
				id: 'duration',
				label: translations.duration.label,
				hint: translations.duration.hint,
				placeholder: ''
			},
			{
				type: 'text',
				name: 'city',
				id: 'city',
				label: translations.city.label,
				placeholder: ''
			},
			{
				type: 'WYSIWYG',
				name: 'description',
				id: 'description',
				cssClasses: 'w-full',
				label: translations.description.label,
				placeholder: translations.description.placeholder
			}
		];
		const slot: SectionSlotInterface = {
			isUnfolded: false,
			formGroup: new UntypedFormGroup({}),
			fields
		};

		slot.fields?.forEach((field: BuilderInputInterface) => {
			if (field.name) {
				slot.formGroup?.addControl(field.name, new UntypedFormControl(''));
			}

			if (field.name === 'duration') {
				slot.formGroup?.addControl(`${field.name}End`, new UntypedFormControl(''));
			}
		});

		this.handleSlotFolding(this._educationHistory.code, 0, false);

		if (this._educationHistory.slots) {
			slot.formGroup?.addControl('index', new UntypedFormControl(this._educationHistory.slots.length));
			return this._educationHistory.slots.push(slot) - 1;
		}

		slot.formGroup?.addControl('index', new UntypedFormControl(0));

		this._educationHistory.slots = [slot];
		return 0;
	}

	addWebsitesSlot(): number {
		const translations = this._t.builder.sections.websitesSocialLinks;
		const fields: BuilderInputInterface[] = [
			{
				type: 'text',
				name: 'name',
				id: 'name',
				label: translations.name.label,
				placeholder: ''
			},
			{
				type: 'text',
				name: 'link',
				id: 'link',
				label: translations.link.label,
				placeholder: ''
			}
		];
		const slot: SectionSlotInterface = {
			isUnfolded: false,
			formGroup: new UntypedFormGroup({}),
			fields
		};

		slot.fields?.forEach((field: BuilderInputInterface) => {
			if (field.name) {
				slot.formGroup?.addControl(field.name, new UntypedFormControl(''));
			}
		});

		this.handleSlotFolding(this._websitesSocialLinks.code, 0, false);

		if (this._websitesSocialLinks.slots) {
			slot.formGroup?.addControl('index', new UntypedFormControl(this._websitesSocialLinks.slots.length));
			return this._websitesSocialLinks.slots.push(slot) - 1;
		}

		slot.formGroup?.addControl('index', new UntypedFormControl(0));

		this._websitesSocialLinks.slots = [slot];
		return 0;
	}

	addSkillsSlot(): number {
		const translations = this._t.builder.sections.skills;
		const fields: BuilderInputInterface[] = [
			{
				type: 'text',
				name: 'skill',
				id: 'skill',
				label: translations.skill.label,
				placeholder: ''
			},
			{
				type: 'range',
				name: 'level',
				max: 4,
				min: 0,
				value: 4,
				step: 1,
				levelText: translations.level['4'],
				id: 'level',
				label: translations.level.label,
				placeholder: ''
			}
		];
		const slot: SectionSlotInterface = {
			isUnfolded: false,
			formGroup: new UntypedFormGroup({}),
			fields
		};

		slot.fields?.forEach((field: BuilderInputInterface) => {
			if (field.name) {
				slot.formGroup?.addControl(field.name, new UntypedFormControl(''));
			}
		});

		this.handleSlotFolding(this._skills.code, 0, false);

		if (this._skills.slots) {
			slot.formGroup?.addControl('index', new UntypedFormControl(this._skills.slots.length));
			return this._skills.slots.push(slot) - 1;
		}

		slot.formGroup?.addControl('index', new UntypedFormControl(0));

		this._skills.slots = [slot];
		return 0;
	}

	addExtraWorkHistorySlot(): number {
		const translations = this._t.builder.sections.extraWorkHistory;
		const fields: BuilderInputInterface[] = [
			{
				type: 'text',
				name: 'postTitle',
				id: 'postTitle',
				label: translations.postTitle.label,
				placeholder: ''
			},
			{
				type: 'text',
				name: 'host',
				id: 'host',
				label: translations.host.label,
				placeholder: ''
			},
			{
				type: 'month',
				name: 'duration',
				id: 'duration',
				label: translations.duration.label,
				hint: translations.duration.hint,
				placeholder: ''
			},
			{
				type: 'text',
				name: 'city',
				id: 'city',
				label: translations.city.label,
				placeholder: ''
			},
			{
				type: 'WYSIWYG',
				name: 'description',
				id: 'description',
				cssClasses: 'w-full',
				label: translations.description.label,
				placeholder: ''
			}
		];
		const slot: SectionSlotInterface = {
			isUnfolded: false,
			formGroup: new UntypedFormGroup({}),
			fields
		};

		slot.fields?.forEach((field: BuilderInputInterface) => {
			if (field.name) {
				slot.formGroup?.addControl(field.name, new UntypedFormControl(''));
			}

			if (field.name === 'duration') {
				slot.formGroup?.addControl(`${field.name}End`, new UntypedFormControl(''));
			}
		});

		this.handleSlotFolding(this._extraWorkHistory.code, 0, false);

		if (this._extraWorkHistory.slots) {
			slot.formGroup?.addControl('index', new UntypedFormControl(this._extraWorkHistory.slots.length));
			return this._extraWorkHistory.slots.push(slot) - 1;
		}

		slot.formGroup?.addControl('index', new UntypedFormControl(0));

		this._extraWorkHistory.slots = [slot];
		return 0;
	}

	addCoursesHistorySlot(): number {
		const translations = this._t.builder.sections.coursesHistory;
		const fields: BuilderInputInterface[] = [
			{
				type: 'text',
				name: 'postTitle',
				id: 'postTitle',
				label: translations.postTitle.label,
				placeholder: ''
			},
			{
				type: 'text',
				name: 'host',
				id: 'host',
				label: translations.host.label,
				placeholder: ''
			},
			{
				type: 'month',
				name: 'duration',
				id: 'duration',
				label: translations.duration.label,
				hint: translations.duration.hint,
				placeholder: ''
			}
		];
		const slot: SectionSlotInterface = {
			isUnfolded: false,
			formGroup: new UntypedFormGroup({}),
			fields
		};

		slot.fields?.forEach((field: BuilderInputInterface) => {
			if (field.name) {
				slot.formGroup?.addControl(field.name, new UntypedFormControl(''));
			}

			if (field.name === 'duration') {
				slot.formGroup?.addControl(`${field.name}End`, new UntypedFormControl(''));
			}
		});

		this.handleSlotFolding(this._coursesHistory.code, 0, false);

		if (this._coursesHistory.slots) {
			slot.formGroup?.addControl('index', new UntypedFormControl(this._coursesHistory.slots.length));
			return this._coursesHistory.slots.push(slot) - 1;
		}

		slot.formGroup?.addControl('index', new UntypedFormControl(0));

		this._coursesHistory.slots = [slot];
		return 0;
	}

	addTrainingsHistorySlot(): number {
		const translations = this._t.builder.sections.trainingsHistory;
		const fields: BuilderInputInterface[] = [
			{
				type: 'text',
				name: 'postTitle',
				id: 'postTitle',
				label: translations.postTitle.label,
				placeholder: ''
			},
			{
				type: 'text',
				name: 'host',
				id: 'host',
				label: translations.host.label,
				placeholder: ''
			},
			{
				type: 'month',
				name: 'duration',
				id: 'duration',
				label: translations.duration.label,
				hint: translations.duration.hint,
				placeholder: ''
			},
			{
				type: 'WYSIWYG',
				name: 'description',
				id: 'description',
				cssClasses: 'w-full',
				label: translations.description.label,
				placeholder: translations.description.placeholder
			}
		];
		const slot: SectionSlotInterface = {
			isUnfolded: false,
			formGroup: new UntypedFormGroup({}),
			fields
		};

		slot.fields?.forEach((field: BuilderInputInterface) => {
			if (field.name) {
				slot.formGroup?.addControl(field.name, new UntypedFormControl(''));
			}

			if (field.name === 'duration') {
				slot.formGroup?.addControl(`${field.name}End`, new UntypedFormControl(''));
			}
		});

		this.handleSlotFolding(this._trainingsHistory.code, 0, false);

		if (this._trainingsHistory.slots) {
			slot.formGroup?.addControl('index', new UntypedFormControl(this._trainingsHistory.slots.length));
			return this._trainingsHistory.slots.push(slot) - 1;
		}

		slot.formGroup?.addControl('index', new UntypedFormControl(0));

		this._trainingsHistory.slots = [slot];
		return 0;
	}

	addLanguagesSlot(): number {
		const translations = this._t.builder.sections.languages;
		const fields: BuilderInputInterface[] = [
			{
				type: 'text',
				name: 'lang',
				id: 'lang',
				label: translations.lang.label,
				placeholder: ''
			},
			{
				type: 'range',
				name: 'level',
				max: 3,
				min: 0,
				value: 3,
				step: 1,
				levelText: translations.level['3'],
				id: 'level',
				label: translations.level.label,
				placeholder: ''
			}
		];
		const slot: SectionSlotInterface = {
			isUnfolded: false,
			formGroup: new UntypedFormGroup({}),
			fields
		};

		slot.fields?.forEach((field: BuilderInputInterface) => {
			if (field.name) {
				slot.formGroup?.addControl(field.name, new UntypedFormControl(field?.value ? field.value : ''));
			}
		});

		this.handleSlotFolding(this._languages.code, 0, false);

		if (this._languages.slots) {
			slot.formGroup?.addControl('index', new UntypedFormControl(this._languages.slots.length));
			return this._languages.slots.push(slot) - 1;
		}

		slot.formGroup?.addControl('index', new UntypedFormControl(0));

		this._languages.slots = [slot];
		return 0;
	}

	addCustomSectionSlot(code: CustomSectionType): number {
		const translations = this._t.builder.sections.customSection;
		const fields: BuilderInputInterface[] = [
			{
				type: 'text',
				name: 'postTitle',
				id: 'postTitle',
				label: translations.postTitle.label,
				placeholder: ''
			},
			{
				type: 'text',
				name: 'city',
				id: 'city',
				label: translations.city.label,
				placeholder: ''
			},
			{
				type: 'month',
				name: 'duration',
				id: 'duration',
				label: translations.duration.label,
				hint: translations.duration.hint,
				placeholder: ''
			},
			{
				type: 'WYSIWYG',
				name: 'description',
				id: 'description',
				cssClasses: 'w-full',
				label: translations.description.label,
				placeholder: ''
			}
		];
		const slot: SectionSlotInterface = {
			isUnfolded: false,
			formGroup: new UntypedFormGroup({}),
			fields
		};

		slot.fields?.forEach((field: BuilderInputInterface) => {
			if (field.name) {
				slot.formGroup?.addControl(field.name, new UntypedFormControl(''));
			}

			if (field.name === 'duration') {
				slot.formGroup?.addControl(`${field.name}End`, new UntypedFormControl(''));
			}
		});

		this.handleSlotFolding(this._otherCustomSections[code].code, 0, false);

		if (
			this._otherCustomSections[code] &&
			this._otherCustomSections[code].slots &&
			this._otherCustomSections[code]?.slots?.length
		) {
			slot.formGroup?.addControl('index', new UntypedFormControl(this._otherCustomSections[code]?.slots?.length));
			return (<SectionSlotInterface[]>this._otherCustomSections[code].slots).push(slot) - 1;
		}

		slot.formGroup?.addControl('index', new UntypedFormControl(0));

		this._otherCustomSections[code].slots = [slot];
		return 0;
	}

	/*
	 * utility methods
	 * */

	addSlotToSectionForm(section: BuilderSectionInterface, index: number): void {
		if (section.slots && section.slots.length) {
			if (!section.formGroup) {
				section.formGroup = new UntypedFormGroup({});
			}

			section.slots[index].formGroup.addControl('slotId', new UntypedFormControl(generateRandomText()));

			section.formGroup.addControl(
				`section_${index}`,
				new UntypedFormControl({
					slot: section.slots[index].formGroup,
					index
				})
			);
		}

		return;
	}

	removeSlotFromSectionForm(section: BuilderSectionInterface, index: number): void {
		if (section.formGroup) {
			Object.keys(section.formGroup.controls).forEach((key: string) => {
				const controlIndex = (<{ slot: UntypedFormGroup; index: number }>section.formGroup?.get(key)?.value).index;
				if (controlIndex === index && section.formGroup) {
					section?.formGroup.removeControl(key);
				}
			});
		}
		this.updateFormIndexes(section, 'section');

		return;
	}

	updateFormIndexes(section: BuilderSectionInterface, concernedFormGroup: 'section' | 'slots'): void {
		if (concernedFormGroup === 'section' && section.formGroup) {
			Object.keys(section.formGroup.controls).forEach((key: string, index: number) => {
				const currentVal = <{ slot: UntypedFormGroup; index: number }>section.formGroup?.get(key)?.value;
				section.formGroup?.get(key)?.patchValue({ ...currentVal, index });
			});
		}

		if (concernedFormGroup === 'slots' && section.slots && section.slots.length && section.formGroup) {
			section.slots.forEach((slot: SectionSlotInterface, index) => {
				slot.formGroup?.get('index')?.patchValue(index);
			});
			Object.keys(section.formGroup.controls).forEach((key: string, index: number) => {
				if (section?.slots && section?.slots[index]) {
					const currentVal = {
						slot: section?.slots[index]?.formGroup,
						index
					};
					section.formGroup?.get(key)?.patchValue(currentVal);
				}
			});
		}

		return;
	}

	refreshSectionFormGroup(code: SectionCodeType): void {
		switch (code) {
			case 'courses-history':
				this.updateFormIndexes(this._coursesHistory, 'slots');
				return;
			case 'trainings-history':
				this.updateFormIndexes(this._trainingsHistory, 'slots');
				return;
			case 'languages':
				this.updateFormIndexes(this._languages, 'slots');
				return;
			case 'extra-work-history':
				this.updateFormIndexes(this._extraWorkHistory, 'slots');
				return;
			case 'skills':
				this.updateFormIndexes(this._skills, 'slots');
				return;
			case 'websites-social-links':
				this.updateFormIndexes(this._websitesSocialLinks, 'slots');
				return;
			case 'employment-history':
				this.updateFormIndexes(this._employmentHistory, 'slots');
				return;
			case 'education-history':
				this.updateFormIndexes(this._educationHistory, 'slots');
				return;
			default:
				if (code.includes('custom-section') && this._otherCustomSections[code]) {
					this.updateFormIndexes(this._otherCustomSections[code], 'slots');
				}
				return;
		}
	}
}
