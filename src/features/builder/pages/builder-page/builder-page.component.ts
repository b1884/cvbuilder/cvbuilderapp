import {
	AfterViewChecked,
	ChangeDetectorRef,
	Component,
	DoCheck,
	ElementRef,
	HostListener,
	KeyValueDiffer,
	KeyValueDiffers,
	OnInit,
	ViewChild
} from '@angular/core';
import { BaseComponent } from '../../../../shared/classes/base-component.class';
import { ShareLinkModalComponent } from '../../../../shared/components/share-link-modal/share-link-modal.component';
import { first } from 'rxjs/operators';
import {
	BuilderForm,
	BuilderInputInterface,
	BuilderSectionInterface,
	InputType,
	SectionCodeType,
	SectionSlotInterface
} from './builder-form.modal';
import {
	ConfirmationModalComponent,
	ConfirmationModalInterface
} from '../../../../shared/components/confirmation-modal/confirmation-modal.component';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import {
	ClassicResumeInterface,
	ClassicResumePreviewType,
	WriteImageResponseType
} from '../../../../types/classic-resume.types';
import { ActivatedRoute, Params } from '@angular/router';
import { getPersonalPhotoURL } from '../../../../core/utils/links.util';
import {
	BuilderImageSizeInterface,
	calculateAspectRatioFit,
	isFileLegal,
	percentOf
} from '../../../../core/utils/images.util';
import { FieldInterface, ResumeInputInterface } from '../../../../types/resume-input.types';
import { LangType } from '../../../../types/lang.types';
import { firstValueFrom, Observable } from 'rxjs';
import { GenericType } from '../../../../types/generic.type';
import { HTMLInputEventInterface } from '../../../../types/HTMLInputEvent.types';
import { saveAs } from 'file-saver';
import { HttpErrorResponse } from '@angular/common/http';
import { SRC_IMG_HEIGHT, SRC_IMG_WIDTH } from '../../../../core/config/def-image-size.config';

@Component({
	selector: 'blackbird-builder-page',
	templateUrl: './builder-page.component.html',
	styleUrls: ['./builder-page.component.scss']
})
export class BuilderPageComponent extends BaseComponent implements AfterViewChecked, OnInit, DoCheck {
	@ViewChild('fileInput') fileInput!: ElementRef<HTMLInputElement>;

	currentResume!: ClassicResumeInterface | null;
	currentResumeLink!: string;
	// currentResumePages: WriteImageResponseType[] = [];
	totalPages = 1;
	currentPageNumber = 1;
	currentPage!: WriteImageResponseType;
	isBuilding = false;

	private _differ: KeyValueDiffer<string, any>;

	private _lowerCaseAt!: string;
	private _upperCaseAt!: string;
	private _formatErrMsg!: string;
	private _dismissBtn!: string;
	private _twoPagesWarning!: string;
	private _sizeErrMsg!: string;

	toPresentLabel!: string;
	toPresentValue!: string;

	builderForm!: BuilderForm;
	editorToolbar!: HTMLElement;
	wysiwygEditorHeight = '';
	extraSectionsButtons: { name: SectionCodeType; button: string }[] = [
		{
			name: 'custom-section',
			button: this.t.builder.extraSections.custom
		},
		{
			name: 'courses-history',
			button: this.t.builder.extraSections.courses
		},
		{
			name: 'extra-work-history',
			button: this.t.builder.extraSections.extraActivities
		},
		{
			name: 'trainings-history',
			button: this.t.builder.extraSections.trainings
		},
		{
			name: 'fun',
			button: this.t.builder.extraSections.fun
		},
		{
			name: 'languages',
			button: this.t.builder.extraSections.languages
		}
	];

	windowHeight = window.innerHeight;
	windowWidth = window.innerWidth;

	builderImageSize: BuilderImageSizeInterface = calculateAspectRatioFit(
		SRC_IMG_WIDTH,
		SRC_IMG_HEIGHT,
		percentOf(40, this.windowWidth),
		percentOf(80, this.windowHeight)
	);

	slotsTitles: GenericType = {};

	private _formChangeTimeout: unknown;

	// datePickerOptions: FlatpickrOptions = {
	// 	allowInput: false,
	// 	dateFormat: 'M-Y',
	// };

	builderAPIPayload!: ClassicResumeInterface;

	constructor(private _cdref: ChangeDetectorRef, private _differs: KeyValueDiffers, private _route: ActivatedRoute) {
		super();
		this._differ = _differs.find({}).create();
	}

	ngDoCheck(): void {
		const change = this._differ.diff(this.builderForm);
		if (change) {
			change.forEachChangedItem((item) => {
				console.log('item changed', item);
			});
		}
	}

	ngAfterViewChecked(): void {
		if (!this.wysiwygEditorHeight) {
			if (!this.editorToolbar) {
				const elements = document.getElementsByClassName('ql-toolbar ql-snow');
				if (elements && elements.item(0)) {
					this.editorToolbar = elements.item(0) as HTMLElement;
				}
			}

			if (this.editorToolbar) {
				this.wysiwygEditorHeight = `calc(100% - ${this.editorToolbar.offsetHeight}px)`;
			}
		}
		this._cdref.detectChanges();
	}

	@HostListener('window:resize')
	refreshBuilderImageSize(): void {
		const windowHeight = window.innerHeight;
		const windowWidth = window.innerWidth;

		if (windowHeight !== this.windowHeight) {
			this.builderImageSize = calculateAspectRatioFit(
				SRC_IMG_WIDTH,
				SRC_IMG_HEIGHT,
				percentOf(45, windowWidth),
				percentOf(80, windowHeight)
			);
		}
	}

	ngOnInit(): void {
		this._route.params.subscribe((value: Params) => {
			if (!value || !value.resumeId) {
				void this.router.navigate([this.routes.dashboard.main]).catch();
				return;
			}

			this.currentResumeLink = value.resumeId as string;
			void this.initCurrentResume();
		});

		void this._getTranslatedStrings();

		this._cdref.detectChanges();
	}

	private async _getTranslatedStrings() {
		this._lowerCaseAt = await firstValueFrom<string>(
			this.translateService.get(this.t.common.misc.at) as Observable<string>
		);
		this._upperCaseAt = await firstValueFrom<string>(
			this.translateService.get(this.t.common.misc.At) as Observable<string>
		);
		this._sizeErrMsg = await firstValueFrom<string>(
			this.translateService.get(this.t.common.errors.fileSize) as Observable<string>
		);
		this._twoPagesWarning = await firstValueFrom<string>(
			this.translateService.get(this.t.builder.error.twoPages) as Observable<string>
		);
		this._formatErrMsg = await firstValueFrom<string>(
			this.translateService.get(this.t.common.errors.fileFormat) as Observable<string>
		);
		this._dismissBtn = await firstValueFrom<string>(
			this.translateService.get(this.t.common.buttons.dismiss) as Observable<string>
		);
		this.toPresentLabel = await firstValueFrom<string>(
			this.translateService.get(this.t.builder.misc.worksHere) as Observable<string>
		);
		this.toPresentValue = await firstValueFrom<string>(
			this.translateService.get(this.t.builder.misc.toPresent) as Observable<string>
		);
	}

	async initCurrentResume(): Promise<void> {
		this.currentResume = await this.builderService.currentResumeAPI(this.currentResumeLink);
		if (!this.currentResume) {
			this.builderForm = new BuilderForm();
			return;
		}

		this.builderForm = new BuilderForm(this.currentResume.rawData);

		// const generatedResume: ClassicGeneratorOutputInterface | null = await this.builderService.buildAPI(
		// 	this._setBuildPayload()
		// );

		this.builderAPIPayload = this._setBuildPayload();

		// this._setPagesImages(generatedResume);
	}

	async updateTitle(title: string): Promise<void> {
		if (!this.currentResume) {
			return;
		}

		this.isBuilding = true;

		await this.builderService.updateResumeAPI({ title, _id: this.currentResume?._id });
		this.currentResume.title = title;
		this.isBuilding = false;
	}

	// private _setPagesImages(res: ClassicGeneratorOutputInterface | null): void {
	// 	if (!res || !res.pagesAsImages) {
	// 		return;
	// 	}
	//
	// 	this.currentResumePages = res?.pagesAsImages || [];
	//
	// 	if (this.currentResumePages.length) {
	// 		if (this.currentResumePages.length > 1) {
	// 			toast(this._twoPagesWarning, {
	// 				severity: 'warning',
	// 				delay: 6000,
	// 				dismissible: true,
	// 				dismiss: this._dismissBtn,
	// 				newestAtTop: false
	// 			});
	// 		}
	//
	// 		this.currentResumePages = this.currentResumePages.map((image: WriteImageResponseType) => {
	// 			image.path = getBuilderImageURL(
	// 				this.currentResume?.userId as string,
	// 				this.currentResume?._id as string,
	// 				image.name as string
	// 			);
	// 			return image;
	// 		});
	//
	// 		if (this.currentResumePages.length === 1 || this.currentPageNumber > this.currentResumePages.length) {
	// 			this.currentPageNumber = 1;
	// 		}
	// 		this.currentPage = this.currentResumePages[this.currentPageNumber - 1];
	// 	}
	//
	// 	this._cdref.detectChanges();
	//
	// 	this.isBuilding = false;
	// }

	handlePaginatorChangePage(page: number): void {
		this.currentPageNumber = page;
	}

	showSlotDeleteConfirmationDialog(code: SectionCodeType, index: number): void {
		this.modalService
			.addModal<ConfirmationModalInterface, boolean>(ConfirmationModalComponent, {
				confirmationText: this.t.modals.deleteSlot.confirm,
				description: this.t.modals.deleteSlot.description,
				cancelText: this.t.modals.deleteSlot.cancel,
				title: this.t.modals.deleteSlot.title,
				reverseButtons: true
			})
			.pipe(first())
			.subscribe((response: boolean) => {
				if (response) {
					this.builderForm.handleSlotDeletion(code, index);
					this.onFormChange();
				}
			});
	}

	showSectionDeleteConfirmationDialog(index: number): void {
		this.modalService
			.addModal<ConfirmationModalInterface, boolean>(ConfirmationModalComponent, {
				confirmationText: this.t.modals.deleteSection.confirm,
				description: this.t.modals.deleteSection.description,
				cancelText: this.t.modals.deleteSection.cancel,
				title: this.t.modals.deleteSection.title,
				reverseButtons: true
			})
			.pipe(first())
			.subscribe((response: boolean) => {
				if (response) {
					this.builderForm.handleSectionDeletion(index);
					this.onFormChange();
				}
			});
	}

	exitBuilder(): void {
		void this.router.navigate([this.routes.dashboard.main]);
	}

	async openShareLinkDialog(): Promise<void> {
		await firstValueFrom(
			this.modalService.addModal<Pick<ClassicResumePreviewType, 'sharingLink'>, void>(ShareLinkModalComponent, {
				sharingLink: this.currentResume?.sharingLink as string
			})
		);
	}

	isRegularInput(fieldType: InputType): boolean {
		return (
			fieldType !== 'profile-picture' &&
			fieldType !== 'WYSIWYG' &&
			fieldType !== 'month' &&
			fieldType !== 'range' &&
			fieldType !== 'textarea' &&
			fieldType !== 'autocomplete' &&
			fieldType !== 'select'
		);
	}

	filterFields(fields: BuilderInputInterface[] | undefined, isFoldable = false): BuilderInputInterface[] {
		return fields ? fields.filter((f) => (isFoldable ? f.isFoldable : !f.isFoldable)) : [];
	}

	sectionExists(code: SectionCodeType): boolean {
		if (code !== 'custom-section') {
			const sectionsFiltered = this.builderForm.builderSections.filter((section) => section.code === code);
			return !!sectionsFiltered.length;
		}

		return false;
	}

	levelTranslation(ev: any, code: SectionCodeType): string {
		const levels: any =
			code === 'skills' ? this.t.builder.sections.skills.level : this.t.builder.sections.languages.level;
		// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
		const level = ev.target.value;
		return levels[level] as string;
	}

	sectionDrop(event: CdkDragDrop<BuilderSectionInterface[]>): void {
		if (event.currentIndex > 1) {
			moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
			this.onFormChange();
		}
	}

	slotDrop(event: CdkDragDrop<SectionSlotInterface[]>, code: SectionCodeType): void {
		if (event.previousContainer === event.container) {
			moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
			this.builderForm.refreshSectionFormGroup(code);
			this.onFormChange();
		}
	}

	setResumeLanguage(lang: LangType): void {
		if (!this.currentResume) {
			return;
		}

		this.currentResume.lang = lang;
		this.toggleDropdown();
		this.onFormChange();
	}

	onFormChange(): void {
		if (this._formChangeTimeout) {
			clearTimeout(this._formChangeTimeout as never);
		}
		this._formChangeTimeout = setTimeout(() => {
			this.builderAPIPayload = this._setBuildPayload();
			// void this.builderService.buildAPI(this._setBuildPayload()).then((res) => {
			// 	console.log(res);
			//
			// 	this._setPagesImages(res);
			// });
		}, 1500);
	}

	private _setBuildPayload(): ClassicResumeInterface {
		const rawData: ResumeInputInterface = {
			title: this.currentResume?.title as string,
			fields: this.builderForm.getBuilderRawValues(),
			language: this.currentResume?.lang as LangType
		};

		// calling it here because this is the best place
		// for the moment till we reformat the code
		this._setSlotsTitles(rawData.fields);

		return {
			...(this.currentResume as ClassicResumeInterface),
			rawData
		};
	}

	updateSectionTitle(title: string, section: BuilderSectionInterface): void {
		this.builderForm.updateBuilderSection(section.code, { ...section, customTitle: title });
		this.onFormChange();
	}

	choosePhoto(): void {
		if (!this.fileInput || !this.fileInput.nativeElement) {
			return;
		}

		this.fileInput.nativeElement.click();
	}

	async uploadPhoto(ev: HTMLInputEventInterface | Event): Promise<void> {
		if (!this.currentResume) {
			return;
		}

		const event: HTMLInputEventInterface = <HTMLInputEventInterface>ev;

		if (!event.target.files || !event.target.files.item(0)) {
			return;
		}

		const file: File = event.target.files.item(0) as File;

		if (!isFileLegal(file, this._sizeErrMsg, this._formatErrMsg)) {
			return;
		}

		this.loaderService.showLoader();

		const { _id: id, userId } = this.currentResume;
		try {
			const uploadResult = await this.builderService.updatePersonalPhotoAPI({ _id: id, userId }, file);

			this.currentResume.profileImageLink = getPersonalPhotoURL(
				this.currentResume.userId,
				uploadResult.profileImage
			);
			this.currentResume.profileImage = uploadResult.profileImage;

			this.onFormChange();

			this.loaderService.hideLoader();
		} catch (e) {
			console.error(e);
			this.loaderService.hideLoader();
		}
	}

	async deletePhoto(): Promise<void> {
		if (!this.currentResume) {
			return;
		}

		const isDeletingConfirmed = await firstValueFrom(
			this.modalService.addModal<ConfirmationModalInterface, boolean>(ConfirmationModalComponent, {
				confirmationText: this.t.modals.deletePhoto.confirm,
				description: this.t.modals.deletePhoto.description,
				cancelText: this.t.modals.deletePhoto.cancel,
				title: this.t.modals.deletePhoto.title,
				reverseButtons: true
			})
		);

		if (!isDeletingConfirmed) {
			return;
		}

		this.loaderService.showLoader();
		try {
			const { _id: id, userId } = this.currentResume;

			await this.builderService.removePersonalPhotoAPI({ _id: id, userId });
			this.currentResume.profileImage = '';
			this.currentResume.profileImageLink = '';
			this.loaderService.hideLoader();
			this.onFormChange();
		} catch (e) {
			console.error(e);
			this.loaderService.hideLoader();
		}
	}

	private _setSlotsTitles(fields: FieldInterface[]): void {
		for (const field of fields) {
			if (field.slots && field.slots.length) {
				for (const slot of field.slots) {
					if (!slot.slotId) {
						return;
					}

					this.slotsTitles[slot.slotId] =
						slot.lang ||
						slot.name ||
						slot.skill ||
						`${slot.postTitle || ''}${
							slot.host ? `${slot.postTitle ? this._lowerCaseAt : this._upperCaseAt} ${slot.host}` : ''
						}`;
				}
			}
		}
	}

	async handleDownloadCv(): Promise<void> {
		this.loaderService.showLoader();

		try {
			const pdfLink: string[] | null = await this.builderService.downloadResumeAPI(
				this.currentResumeLink,
				this.currentResume?.userId as string,
				this.currentResume?._id as string
			);

			if (!pdfLink) {
				return;
			}

			saveAs(pdfLink[1], pdfLink[0]);
			this.loaderService.hideLoader();
		} catch (e) {
			this.loaderService.hideLoader();
			if (!e) {
				return;
			}

			if (e instanceof HttpErrorResponse) {
				alert(e.error.message);
			}
		}
	}
}
