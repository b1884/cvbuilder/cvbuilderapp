import { BuilderSections } from './builder-sections.class';
import { UntypedFormGroup } from '@angular/forms';
import { FieldInterface, ResumeInputInterface } from '../../../../types/resume-input.types';

export type InputType =
	| 'profile-picture'
	| 'text'
	| 'email'
	| 'tel'
	| 'date'
	| 'month'
	| 'textarea'
	| 'WYSIWYG'
	| 'range'
	| 'select'
	| 'autocomplete';

export type CustomSectionType = `custom-section-${number}`;

export type SectionCodeType =
	| 'personal-details'
	| 'personal-resume'
	| 'employment-history'
	| 'education-history'
	| 'websites-social-links'
	| 'skills'
	| 'courses-history'
	| 'trainings-history'
	| 'languages'
	| 'fun'
	| 'extra-work-history'
	| 'custom-section'
	| CustomSectionType;

export interface SelectOptionsInterface {
	value: string;
	label: string;
}

// NB: Label is not optional even though there are some fields that don't need it
// because we face some errors, and the only clean way to prevent them for the moment
// is { ... label: '' ... }
export interface BuilderInputInterface {
	type: InputType;
	cssClasses?: string;
	label: string;
	isFocused?: boolean;
	isFoldable?: boolean;
	placeholder: string;
	name: string;
	hint?: string;
	tip?: string;
	min?: number;
	max?: number;
	value?: string | number;
	levelText?: string;
	step?: number;
	id?: string;
	options?: SelectOptionsInterface[];
}

export interface SectionSlotInterface {
	writtenTitle?: string;
	isUnfolded?: boolean;
	writtenDuration?: string;
	formGroup: UntypedFormGroup;
	fields?: BuilderInputInterface[];
}

export interface BuilderSectionInterface {
	title: string;
	customTitle?: string;
	code: SectionCodeType;
	addSlot?: string;
	hint?: string;
	draggable?: boolean;
	isUnfolded?: boolean;
	isDeletable?: boolean;
	isFoldable?: boolean;
	description?: string;
	formGroup: UntypedFormGroup;
	fields?: BuilderInputInterface[];
	slots?: SectionSlotInterface[];
}

export class BuilderForm extends BuilderSections {
	private _builderSections: BuilderSectionInterface[] = [];

	constructor(rawData?: ResumeInputInterface) {
		super(rawData);
		this._initSections(rawData);
	}

	get builderSections(): BuilderSectionInterface[] {
		return this._builderSections;
	}

	set builderSections(value: BuilderSectionInterface[]) {
		this._builderSections = [...this._builderSections, ...value];
	}

	updateBuilderSection(sectionCode: SectionCodeType, section: Partial<BuilderSectionInterface>): void {
		const sectionIndex = this._builderSections.findIndex((section) => section.code === sectionCode);

		if (sectionIndex < 0) {
			return;
		}

		this._builderSections[sectionIndex] = {
			...this._builderSections[sectionIndex],
			...section
		};
	}

	private _initSections(rawData?: ResumeInputInterface): void {
		this.builderSections = [this.personalDetails, this.personalResume];

		if (!rawData) {
			this.builderSections = [
				this.employmentHistory,
				this.educationHistory,
				this.websitesSocialLinks,
				this.skills
			];

			return;
		}

		const defaultSections: SectionCodeType[] = [
			'employment-history',
			'education-history',
			'websites-social-links',
			'skills'
		];

		const submittedSections = rawData?.fields;

		for (const section of submittedSections) {
			if (!defaultSections.includes(section.code)) {
				this.addSection(section.code);
				continue;
			}

			for (const defSection of defaultSections) {
				if (defSection !== section.code) {
					continue;
				}

				switch (defSection) {
					case 'employment-history':
						this.builderSections = [this.employmentHistory];
						break;
					case 'education-history':
						this.builderSections = [this.educationHistory];
						break;
					case 'websites-social-links':
						this.builderSections = [this.websitesSocialLinks];
						break;
					case 'skills':
						this.builderSections = [this.skills];
						break;
					default:
						break;
				}
			}
		}

		for (const defSection of defaultSections) {
			const sectionIndex = submittedSections?.findIndex((x) => x.code === defSection);

			if (sectionIndex >= 0) {
				continue;
			}

			switch (defSection) {
				case 'education-history':
					this.builderSections = [this.educationHistory];
					break;
				case 'employment-history':
					this.builderSections = [this.employmentHistory];
					break;
				case 'websites-social-links':
					this.builderSections = [this.websitesSocialLinks];
					break;
				case 'skills':
					this.builderSections = [this.skills];
					break;
				default:
					break;
			}
		}
	}

	handleSectionDeletion(index: number): void {
		this._builderSections.splice(index, 1);
	}

	addSlot(code: SectionCodeType): void {
		let newSlotIndex = 0;
		switch (code) {
			case 'courses-history':
				newSlotIndex = this.addCoursesHistorySlot();
				this.handleSlotsFormsAddition(code, newSlotIndex);
				setTimeout(() => this.handleSlotFolding(code, newSlotIndex, true), 333);
				return;
			case 'trainings-history':
				newSlotIndex = this.addTrainingsHistorySlot();
				this.handleSlotsFormsAddition(code, newSlotIndex);
				setTimeout(() => this.handleSlotFolding(code, newSlotIndex, true), 333);
				return;
			case 'languages':
				newSlotIndex = this.addLanguagesSlot();
				this.handleSlotsFormsAddition(code, newSlotIndex);
				setTimeout(() => this.handleSlotFolding(code, newSlotIndex, true), 333);
				return;
			case 'extra-work-history':
				newSlotIndex = this.addExtraWorkHistorySlot();
				this.handleSlotsFormsAddition(code, newSlotIndex);
				setTimeout(() => this.handleSlotFolding(code, newSlotIndex, true), 333);
				return;
			case 'skills':
				newSlotIndex = this.addSkillsSlot();
				this.handleSlotsFormsAddition(code, newSlotIndex);
				setTimeout(() => this.handleSlotFolding(code, newSlotIndex, true), 333);
				return;
			case 'websites-social-links':
				newSlotIndex = this.addWebsitesSlot();
				this.handleSlotsFormsAddition(code, newSlotIndex);
				setTimeout(() => this.handleSlotFolding(code, newSlotIndex, true), 333);
				return;
			case 'employment-history':
				newSlotIndex = this.addEmploymentSlot();
				this.handleSlotsFormsAddition(code, newSlotIndex);
				setTimeout(() => this.handleSlotFolding(code, newSlotIndex, true), 333);
				return;
			case 'education-history':
				newSlotIndex = this.addEducationSlot();
				this.handleSlotsFormsAddition(code, newSlotIndex);
				setTimeout(() => this.handleSlotFolding(code, newSlotIndex, true), 333);
				return;
			default:
				if (code.includes('custom-section')) {
					newSlotIndex = this.addCustomSectionSlot(code as CustomSectionType);
					this.handleSlotsFormsAddition(code, newSlotIndex);
					setTimeout(() => this.handleSlotFolding(code, newSlotIndex, true), 333);
				}
				return;
		}
	}

	addSection(code: SectionCodeType): void {
		let sectionCode = '';
		switch (code) {
			case 'custom-section':
				sectionCode = this.generateNewCustomSection();
				this.builderSections = [this.otherCustomSections[sectionCode]];
				return;
			case 'courses-history':
				this.builderSections = [this.coursesHistory];
				return;
			case 'trainings-history':
				this.builderSections = [this.trainingsHistory];
				return;
			case 'languages':
				this.builderSections = [this.languages];
				return;
			case 'fun':
				this.builderSections = [this.fun];
				return;
			case 'extra-work-history':
				this.builderSections = [this.extraWorkHistory];
				return;
			default:
				if (code.includes('custom-section-')) {
					this.builderSections = [this.otherCustomSections[code]];
				}
				return;
		}
	}

	getBuilderRawValues(): FieldInterface[] {
		return this._builderSections.map((section: BuilderSectionInterface) => {
			let secFormGroup: FieldInterface = { code: section.code, title: section.title, slots: [] };
			const slots: any[] = [];
			if (section.formGroup) {
				// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
				secFormGroup = section.slots?.length ? {} : section.formGroup.getRawValue();
				secFormGroup.code = section.code;
				secFormGroup.title = section.title;
				secFormGroup.customTitle = section.customTitle || undefined;

				if (section.formGroup.controls && section.slots?.length) {
					Object.keys(section.formGroup.controls).forEach((key: string) => {
						if (section.formGroup.get(key) && section.formGroup.get(key)?.value?.slot) {
							slots.push((<UntypedFormGroup>section.formGroup.get(key)?.value?.slot).getRawValue());
						}
					});

					if (slots.length) {
						secFormGroup = { ...secFormGroup, slots };
					}
				}
			}
			return secFormGroup;
		});
	}
}
