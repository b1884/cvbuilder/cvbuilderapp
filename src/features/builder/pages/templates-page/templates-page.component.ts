import { ChangeDetectorRef, Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '../../../../shared/classes/base-component.class';
import { ShareLinkModalComponent } from '../../../../shared/components/share-link-modal/share-link-modal.component';
import { first } from 'rxjs/operators';
import {
	TemplatesMobileModalComponent,
	TemplatesMobileModalInterface
} from '../../components/templates-mobile-modal/templates-mobile-modal.component';
import { PubSubInterface } from '../../../../types/pub-sub.type';
import { PubSubTopicsEnum } from '../../../../core/enums/pub-sub-topics.enum';
import { ActivatedRoute, Params } from '@angular/router';
import { TemplateInterface } from '../../../../types/template.types';
import {
	ClassicGeneratorOutputInterface,
	ClassicResumeInterface,
	ClassicResumePreviewType,
	WriteImageResponseType
} from '../../../../types/classic-resume.types';
import { getBuilderImageURL } from '../../../../core/utils/links.util';
import { saveAs } from 'file-saver';
import { firstValueFrom } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { BuilderImageSizeInterface, calculateAspectRatioFit, percentOf } from '../../../../core/utils/images.util';
import { SRC_IMG_HEIGHT, SRC_IMG_WIDTH } from '../../../../core/config/def-image-size.config';

@Component({
	selector: 'blackbird-templates-page',
	templateUrl: './templates-page.component.html',
	styleUrls: ['./templates-page.component.scss']
})
export class TemplatesPageComponent extends BaseComponent implements OnInit {
	@ViewChild('templatesList') templatesList!: ElementRef<HTMLElement>;
	currentResumeLink = '';
	currentPage!: WriteImageResponseType;
	currentPageNumber = 1;
	templates: TemplateInterface[] | null = [];
	currentResume!: ClassicResumeInterface | null;
	isBuilding = false;

	builderAPIPayload!: ClassicResumeInterface;
	totalPages = 1;

	windowHeight = window.innerHeight;
	windowWidth = window.innerWidth;

	builderImageSize: BuilderImageSizeInterface = this._calculateAspRatioBasedOnBreakPoint(
		this.windowWidth,
		this.windowHeight
	);

	private _selectTemplatesTimeout: unknown;

	constructor(private _route: ActivatedRoute, private _cdref: ChangeDetectorRef) {
		super();
	}

	@HostListener('window:resize')
	refreshBuilderImageSize(): void {
		const windowHeight = window.innerHeight;
		const windowWidth = window.innerWidth;

		if (windowHeight !== this.windowHeight || windowWidth !== this.windowWidth) {
			this.builderImageSize = this._calculateAspRatioBasedOnBreakPoint(windowWidth, windowHeight);
		}
	}

	private _calculateAspRatioBasedOnBreakPoint(windowWidth: number, windowHeight: number): BuilderImageSizeInterface {
		if (windowWidth <= 888) {
			const newWidth = windowWidth - 130;
			return calculateAspectRatioFit(SRC_IMG_WIDTH, SRC_IMG_HEIGHT, newWidth, newWidth * 2);
		}

		return calculateAspectRatioFit(
			SRC_IMG_WIDTH,
			SRC_IMG_HEIGHT,
			percentOf(60, windowWidth),
			percentOf(100, windowHeight)
		);
	}

	ngOnInit(): void {
		this.pubSubService.sub().subscribe((subSubEvent: PubSubInterface) => {
			if (subSubEvent.topic === PubSubTopicsEnum.changeCvTemplate) {
				void this.selectTemplate(subSubEvent.data as string);
			}
		});

		this._route.params.subscribe((value: Params) => {
			if (!value || !value.resumeId) {
				void this.router.navigate([this.routes.dashboard.main]).catch();
				return;
			}

			this.currentResumeLink = value.resumeId as string;
			void this._initTemplates();
		});
	}

	private _buildResume(templateCodeName?: string) {
		this.isBuilding = true;
		if (!this.currentResume) {
			return;
		}

		this.builderAPIPayload = {
			...this.currentResume,
			templateCodeName: templateCodeName || this.currentResume.templateCodeName
		};

		// const generatedResume: ClassicGeneratorOutputInterface | null = await this.builderService.buildAPI(payload);
		//
		// this._setPagesImages(generatedResume);
	}

	private async _initTemplates(): Promise<void> {
		this.isBuilding = true;
		this.templates = await this.builderService.getTemplatesListAPI();

		if (!this.templates) {
			throw new Error('No templates!');
		}

		this.currentResume = await this.builderService.currentResumeAPI(this.currentResumeLink);

		if (!this.currentResume) {
			throw new Error('No current resume!');
		}

		this.selectTemplate(this.currentResume.templateCodeName);
		this.scrollToSelected();
	}

	scrollToSelected(): void {
		setTimeout(() => {
			if (!this.templates) {
				return;
			}

			const selectedTemplate = this.templates.filter((template: TemplateInterface) => template.selected)[0];

			if (!selectedTemplate) {
				return;
			}

			const templatePreview = document.getElementById(selectedTemplate.name);

			if (templatePreview) {
				this.templatesList.nativeElement.scrollTo({
					top: templatePreview.offsetTop - 200,
					behavior: 'smooth'
				});
			}
		});
	}

	async openShareLinkDialog(): Promise<void> {
		await firstValueFrom(
			this.modalService.addModal<Pick<ClassicResumePreviewType, 'sharingLink'>, void>(ShareLinkModalComponent, {
				sharingLink: this.currentResume?.sharingLink as string
			})
		);
	}

	async downloadResume(): Promise<void> {
		if (!this.currentResume) {
			return;
		}

		this.loaderService.showLoader();

		try {
			const pdfLink: string[] | null = await this.builderService.downloadResumeAPI(
				this.currentResumeLink,
				this.currentResume.userId,
				this.currentResume._id as string
			);

			if (!pdfLink) {
				return;
			}

			saveAs(pdfLink[1], pdfLink[0]);
			this.loaderService.hideLoader();
		} catch (e) {
			this.loaderService.hideLoader();
			if (!e) {
				return;
			}

			if (e instanceof HttpErrorResponse) {
				alert(e.error.message);
			}
		}
	}

	openTemplatesList(): void {
		if (!this.templates) {
			return;
		}

		this.modalService
			.addModal<TemplatesMobileModalInterface, void>(TemplatesMobileModalComponent, { templates: this.templates })
			.pipe(first())
			.subscribe(() => this.scrollToSelected());
	}

	selectTemplate(templateCodeName: string) {
		if (this._selectTemplatesTimeout) {
			clearTimeout(this._selectTemplatesTimeout as never);
		}

		if (!this.templates) {
			return;
		}

		this.templates = this.templates.map((template) => ({
			...template,
			selected: template.codeName.includes(templateCodeName)
		}));

		this._selectTemplatesTimeout = setTimeout(() => {
			this._buildResume(templateCodeName);
		}, 1500);
	}

	handlePaginatorChangePage(page: number): void {
		this.currentPageNumber = page;
	}
}
