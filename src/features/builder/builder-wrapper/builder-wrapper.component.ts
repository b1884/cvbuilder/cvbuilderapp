import { Component } from '@angular/core';

@Component({
	selector: 'blackbird-builder-wrapper',
	templateUrl: './builder-wrapper.component.html',
	styleUrls: ['./builder-wrapper.component.scss']
})
export class BuilderWrapperComponent {}
