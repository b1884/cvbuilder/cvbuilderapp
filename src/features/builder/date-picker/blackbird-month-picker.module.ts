import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ClickOutsideDirective } from './click-outside.directive';

import { BlackbirdMonthPickerComponent } from './blackbird-month-picker.component';
import { CommonModule } from '@angular/common';
import { OverlayModule } from '@angular/cdk/overlay';

@NgModule({
	declarations: [BlackbirdMonthPickerComponent, ClickOutsideDirective],
	imports: [CommonModule, FormsModule, OverlayModule],
	exports: [BlackbirdMonthPickerComponent]
})
export class BlackbirdMonthPickerModule {}
