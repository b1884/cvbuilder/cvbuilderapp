/* eslint-disable */

import {
  Component,
  OnInit,
  Input,
  forwardRef,
  Output,
  EventEmitter,
  ChangeDetectorRef,
  OnChanges, SimpleChanges
} from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import * as moment from 'moment';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'blackbird-month-picker',
  templateUrl: './blackbird-month-picker.component.html',
  styleUrls: ['./blackbird-month-picker.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => BlackbirdMonthPickerComponent),
      multi: true
    }
  ]
})
export class BlackbirdMonthPickerComponent implements OnInit, ControlValueAccessor, OnChanges {
  @Output() onDateChange: EventEmitter<string | null> = new EventEmitter<string | null>();
  /**
   * Default locale
   */
  @Input() isToPresentEnabled = false;
  /**
   * Default locale
   */
  @Input() locale = 'fr';

  /**
   * Default input class
   */
  @Input() inputClass = 'form-control';

  @Input() toPresentValuePath!: string;

  @Input() toPresentValueText!: string;

  @Input() toPresentValueLabel!: string;

  /**
   * Input display format
   */
  @Input() displayFormat = 'YYYY/MM';

  /**
   * Input value format
   */
  @Input() valueFormat = 'YYYY-MM';

  /**
   * Show calendar icon
   */
  @Input() showIcon = true;

  /**
   * Is control invalid
   */
  @Input() isInvalid = false;

  /**
   * Open month picker flag
   */
  isOpen = false;

  isToPresent = false;

  /**
   * Months array
   */
  months: any[] = [];

  /**
   * Selected year
   */
  selectedYear: any;

  /**
   * Selected month
   */
  selectedMonth: any;

  /**
   * Display value to show in input
   */
  displayValue: any;

  /**
   * Value to be sent in form control
   */
  controlValue: any;

  onChange!: (value?: string | null) => {
  };

  onTouch!: (value?: string | null) => {
  };

  /**
   * Constructor
   */
  constructor(private _cdRef: ChangeDetectorRef) {
  }

  /**
   * @ignore
   */
  ngOnInit() {
    this.selectedYear = new Date().getFullYear();
    this.generateMonths();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.locale) {
      this.generateMonths();
    }

    if (changes.displayValue) {
      const isAllowedValue = changes.displayValue.currentValue === this.toPresentValueText || changes.displayValue.currentValue === this.controlValue;
      if (!isAllowedValue) {
        if (this.controlValue === this.toPresentValuePath) {
          this.displayValue = this.toPresentValueText;
          return;
        }

        this.displayValue = this.controlValue;
      }
    }
  }

  handleDisplayValueChanges(): void {
    setTimeout(() => {
      if (!this.displayValue || !this.displayValue.length) {
        this.controlValue = null;
        this.onChange(this.controlValue);
        this.onTouch(this.controlValue);
        this.onDateChange.emit(null);
        return;
      }

      const isAllowedValue = this.displayValue === this.toPresentValueText || this.displayValue === this.controlValue;
      if (!isAllowedValue) {
        if (this.controlValue === this.toPresentValuePath) {
          this.displayValue = this.toPresentValueText;
          return;
        }

        this.displayValue = moment(this.controlValue).locale(this.locale === 'ar' ? 'en' : this.locale).format(this.displayFormat);
      }
    }, 1200);
  }

  writeValue(value: moment.MomentInput | string) {
    if (!value) {
      return;
    }

    if (typeof value === 'string' && value === this.toPresentValuePath) {
      this.displayValue = this.toPresentValueText;
      this.controlValue = this.toPresentValuePath;
      this.isToPresent = true;
      return;
    }

    this.displayValue = moment(value).locale(this.locale === 'ar' ? 'en' : this.locale).format(this.displayFormat);
    this.controlValue = moment(value).locale(this.locale === 'ar' ? 'en' : this.locale).format(this.valueFormat);
    this.setSelectedYearMonth();
  }

  /**
   * Register on change value
   * @param fn
   */
  registerOnChange(fn: any) {
    this.onChange = fn;
  }

  /**
   * Register on touch value
   * @param fn
   */
  registerOnTouched(fn: any) {
    this.onTouch = fn;
  }

  /**
   * Select month
   * @param selectedMonth
   */
  selectMonth(selectedMonth: any) {
    this.isToPresent = false;
    this.selectedMonth = selectedMonth;
    this.setValue();
    this.isOpen = false;
  }

  selectYearOnly(): void {
    this.isToPresent = false;
    this.setValue(true);
    this.isOpen = false;
  }

  setToPresent() {
    if (this.isToPresent) {
      this.displayValue = this.toPresentValueText;
      this.controlValue = this.toPresentValuePath;
    }

    if (!this.isToPresent) {
      this.setValue();
      this.isOpen = false;
      return;
    }

    this.onChange(this.controlValue);
    this.onTouch(this.controlValue);
    this.onDateChange.emit(this.controlValue);
    this.isOpen = false;
  }

  /**
   * Set selected value to display and form
   */
  setValue(isYearOnly = false) {
    const newDate = `${this.selectedYear}-${this.selectedMonth}`;

    if (this.selectedYear && isYearOnly) {
      this.displayValue = `${this.selectedYear}`;
      this.controlValue = `${this.selectedYear}`;
    }

    if (this.selectedMonth && !isYearOnly) {
      this.displayValue = moment(newDate).locale(this.locale === 'ar' ? 'en' : this.locale).format(this.displayFormat);
      this.controlValue = moment(newDate).locale(this.locale === 'ar' ? 'en' : this.locale).format(this.valueFormat);
      this.setSelectedYearMonth();
    }

    this.onChange(this.controlValue);
    this.onTouch(this.controlValue);
    this.onDateChange.emit(this.controlValue);
  }

  /**
   * On foucus mark as touched
   */
  onFocus() {
    this.onTouch(this.controlValue);
  }

  /**
   * Open close month picker
   */
  toggle() {
    this.isOpen = !this.isOpen;
  }

  /**
   * Set selected year and selected month
   */
  setSelectedYearMonth() {
    if (this.controlValue && this.controlValue !== this.toPresentValuePath) {
      this.selectedYear = moment(this.controlValue).locale(this.locale === 'ar' ? 'en' : this.locale).format('YYYY');
      this.selectedMonth = moment(this.controlValue).locale(this.locale === 'ar' ? 'en' : this.locale).format('MM');
    }
  }

  /**
   * Increase year
   */
  incrementYear() {
    this.selectedYear++;
  }

  /**
   * Decrease year
   */
  decrementYear() {
    this.selectedYear--;
  }

  /**
   * Generate total number of months
   */
  generateMonths() {
    moment.locale(this.locale)
    const months = moment.months().map((month) => ({
        value: moment().month(month).format('MM'),
        name: month
      }));
    this.splitMonths(months, 3);
  }

  /**
   * Create month row array for displaying
   * @param months
   * @param part
   */
  splitMonths(months: any[], part: number) {
    this.months = [];
    while (months.length > 0) {
      // @ts-ignore
      const splittedMonts = months.splice(months, part);
      this.months.push(splittedMonts);
    }
  }
}
