import { Directive, ElementRef, Output, HostListener, EventEmitter, Input } from '@angular/core';

/**
 * Directive to notify when clicked outside of a selective element
 */
@Directive({
	// tslint:disable-next-line:directive-selector
	selector: '[blackbirdClickOutside]'
})
export class ClickOutsideDirective {
	/**
	 * Flag to emit value
	 */
	@Input()
	public triggered = false;

	/**
	 * Event emmiter to notify
	 */
	@Output()
	public clickOutside = new EventEmitter<MouseEvent>();

	/**
	 * Constructor:
	 * @param _elementRef
	 */
	constructor(private _elementRef: ElementRef) {}

	/**
	 * Listen click event and check if the clicked happens inside the required element
	 */
	@HostListener('document:click', ['$event', '$event.target'])
	public onClick(event: MouseEvent, targetElement: HTMLElement): void {
		if (!targetElement) {
			return;
		}

		// Trigger flag reduce emitting, it emits when needed
		if (this.triggered) {
			// eslint-disable-next-line @typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-assignment
			const clickedInside = this._elementRef.nativeElement.contains(targetElement);
			if (!clickedInside) {
				this.clickOutside.emit(event);
			}
		}
	}
}
