import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TemplatesPageComponent } from './pages/templates-page/templates-page.component';
import { BuilderPageComponent } from './pages/builder-page/builder-page.component';
import { TemplatesNavBarComponent } from './components/templates-nav-bar/templates-nav-bar.component';
import { SelectTemplateCardComponent } from './components/select-template-card/select-template-card.component';
import { UploadPhotoModalComponent } from './components/upload-photo-modal/upload-photo-modal.component';
import { BuilderWrapperComponent } from './builder-wrapper/builder-wrapper.component';
import { BuilderRouting } from './builder.routing';
import { SharedModule } from '../../shared/shared.module';
import { TemplatesMobileModalComponent } from './components/templates-mobile-modal/templates-mobile-modal.component';
import { CvPaginatorComponent } from './components/cv-paginator/cv-paginator.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { QuillModule } from 'ngx-quill';
import { ReactiveFormsModule } from '@angular/forms';
import { BlackbirdMonthPickerModule } from './date-picker/blackbird-month-picker.module';
import { DocumentPreviewComponent } from './components/document-preview/document-preview.component';

@NgModule({
	declarations: [
		TemplatesPageComponent,
		BuilderPageComponent,
		TemplatesNavBarComponent,
		SelectTemplateCardComponent,
		UploadPhotoModalComponent,
		BuilderWrapperComponent,
		TemplatesMobileModalComponent,
		CvPaginatorComponent,
		DocumentPreviewComponent
	],
	imports: [
		CommonModule,
		BuilderRouting,
		SharedModule,
		DragDropModule,
		BlackbirdMonthPickerModule,
		QuillModule.forRoot({
			modules: {
				syntax: true,
				toolbar: [
					['bold', 'italic'],
					[{ list: 'ordered' }, { list: 'bullet' }]
				]
			}
		}),
		ReactiveFormsModule
	]
})
export class BuilderModule {}
