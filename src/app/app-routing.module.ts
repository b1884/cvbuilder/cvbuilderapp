import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

const appRoutes: Routes = [
	{
		path: '',
		runGuardsAndResolvers: 'always',
		loadChildren: () => import('../features/dashboard/dashboard.module').then((value) => value.DashboardModule)
	},
	{
		path: 'builder',
		runGuardsAndResolvers: 'always',
		loadChildren: () => import('../features/builder/builder.module').then((value) => value.BuilderModule)
	},
	{
		path: '**',
		redirectTo: ''
	}
];

@NgModule({
	imports: [
		RouterModule.forRoot(appRoutes, {
			onSameUrlNavigation: 'reload'
		})
	],
	exports: [RouterModule]
})
export class AppRoutingModule {}
