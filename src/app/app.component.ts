import { Component, Inject, Injector, OnDestroy, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ServiceLocator } from '../shared/classes/service-locator.class';
import { CookieService } from 'ngx-cookie-service';
import { LANGUAGES } from '../core/config/enabled-languages.config';
import { DOCUMENT } from '@angular/common';
import { Subject, takeUntil } from 'rxjs';

@Component({
	selector: 'blackbird-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
	private _onLangChange: Subject<void> = new Subject<void>();

	constructor(
		private _translateService: TranslateService,
		private _injector: Injector,
		private _cookieService: CookieService,
		@Inject(DOCUMENT) private _doc: Document
	) {
		ServiceLocator.injector = this._injector;
		const cookiesLang = _cookieService.get('x-client-language');
		_translateService.setDefaultLang(
			LANGUAGES.includes(cookiesLang) ? cookiesLang : (_translateService.getBrowserLang() as string)
		);
	}

	ngOnInit(): void {
		const currentLang = this._translateService.currentLang || this._translateService.defaultLang;
		this._doc.documentElement.lang = `${currentLang}`;
		this._doc.documentElement.dir = currentLang === 'ar' ? 'rtl' : 'ltr';

		this._translateService.onLangChange.pipe(takeUntil(this._onLangChange)).subscribe(() => {
			const lang = this._translateService.currentLang || this._translateService.defaultLang;
			this._doc.documentElement.lang = `${lang}`;
			this._doc.documentElement.dir = lang === 'ar' ? 'rtl' : 'ltr';
		});
	}

	ngOnDestroy(): void {
		if (this._onLangChange) {
			this._onLangChange.next();
			this._onLangChange.complete();
		}
	}
}
