import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { SharedModule, WebpackTranslateLoader } from '../shared/shared.module';
import { TranslationPathsService } from '../core/services/translation-paths.service';
import { PubSubService } from '../core/services/pub-sub.service';
import { defaultSimpleModalOptions, SimpleModalModule } from 'ngx-simple-modal';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NetworkService } from '../core/services/network.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { InOutInterceptor } from '../core/services/in-out.interceptor';
import { AuthService } from '../core/services/auth.service';
import { LoaderService } from '../core/services/loader.service';
import { UsersService } from '../core/services/users.service';
import { TitleAndMetaService } from '../core/services/title-and-meta.service';
import { BuilderService } from '../core/services/builder.service';
import { ClassicResumeLinkGuard } from '../features/builder/guards/classic-resume-link.guard';
import { DocumentPreviewManagerService } from '../core/services/document-preview-manager.service';

@NgModule({
	declarations: [AppComponent],
	imports: [
		BrowserModule,
		AppRoutingModule,
		SharedModule,
		BrowserAnimationsModule,
		SimpleModalModule.forRoot(
			{
				container: document.body
			},
			{
				...defaultSimpleModalOptions,
				...{
					closeOnEscape: true,
					closeOnClickOutside: true,
					wrapperDefaultClasses: 'modal fade-anim',
					wrapperClass: 'in',
					animationDuration: 500,
					autoFocus: true
				}
			}
		),
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useClass: WebpackTranslateLoader
			}
		})
	],
	providers: [
		TranslationPathsService,
		PubSubService,
		NetworkService,
		AuthService,
		LoaderService,
		UsersService,
		TitleAndMetaService,
		BuilderService,
		ClassicResumeLinkGuard,
		DocumentPreviewManagerService,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: InOutInterceptor,
			multi: true
		}
	],
	bootstrap: [AppComponent]
})
export class AppModule {}
