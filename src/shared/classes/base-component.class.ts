import { BreakpointObserver } from '@angular/cdk/layout';
import { Router } from '@angular/router';
import { TranslationPathsService } from '../../core/services/translation-paths.service';
import { ServiceLocator } from './service-locator.class';
import appRoutes from '../../routes';
import { PubSubService } from '../../core/services/pub-sub.service';
import { SimpleModalService } from 'ngx-simple-modal';
import { Location } from '@angular/common';
import { AuthService } from '../../core/services/auth.service';
import { LoaderService } from '../../core/services/loader.service';
import { environment } from '../../environments/environment';
import { BuilderService } from '../../core/services/builder.service';
import { TranslateService } from '@ngx-translate/core';
import { UsersService } from '../../core/services/users.service';
import { LandingModuleRoutesInterface } from '../../routes/facade.routes';
import { LangType } from '../../types/lang.types';

export abstract class BaseComponent {
	private _isDroppedDown = false;

	private _backDrop!: HTMLElement | undefined;

	private _lang: string | undefined = localStorage.getItem('lang')
		? (localStorage.getItem('lang') as string | undefined)
		: 'fr';

	private readonly _breakpointObserver: BreakpointObserver;

	private readonly _location: Location;

	private readonly _modalService: SimpleModalService;

	private readonly _router: Router;

	private readonly _pubSubService: PubSubService;

	private readonly _t: TranslationPathsService;

	private readonly _authService: AuthService;

	private readonly _usersService: UsersService;

	private readonly _loaderService: LoaderService;

	private readonly _translateService: TranslateService;

	private readonly _builderService: BuilderService;

	private readonly _routes = appRoutes;

	protected constructor() {
		this._builderService = ServiceLocator.injector.get<BuilderService>(BuilderService);
		this._location = ServiceLocator.injector.get<Location>(Location);
		this._pubSubService = ServiceLocator.injector.get<PubSubService>(PubSubService);
		this._breakpointObserver = ServiceLocator.injector.get<BreakpointObserver>(BreakpointObserver);
		this._t = ServiceLocator.injector.get<TranslationPathsService>(TranslationPathsService);
		this._router = ServiceLocator.injector.get<Router>(Router);
		this._modalService = ServiceLocator.injector.get<SimpleModalService>(SimpleModalService);
		this._authService = ServiceLocator.injector.get<AuthService>(AuthService);
		this._usersService = ServiceLocator.injector.get<UsersService>(UsersService);
		this._loaderService = ServiceLocator.injector.get<LoaderService>(LoaderService);
		this._translateService = ServiceLocator.injector.get<TranslateService>(TranslateService);
	}

	get usersService(): UsersService {
		return this._usersService;
	}

	get translateService(): TranslateService {
		return this._translateService;
	}

	get builderService(): BuilderService {
		return this._builderService;
	}

	get loaderService(): LoaderService {
		return this._loaderService;
	}

	get authService(): AuthService {
		return this._authService;
	}

	get isDroppedDown(): boolean {
		return this._isDroppedDown;
	}

	set isDroppedDown(value: boolean) {
		this._isDroppedDown = value;
	}

	get backDrop(): HTMLElement | undefined {
		return this._backDrop;
	}

	set backDrop(value: HTMLElement | undefined) {
		this._backDrop = value;
	}

	get location(): Location {
		return this._location;
	}

	get modalService(): SimpleModalService {
		return this._modalService;
	}

	get pubSubService(): PubSubService {
		return this._pubSubService;
	}

	get breakpointObserver(): BreakpointObserver {
		return this._breakpointObserver;
	}

	get router(): Router {
		return this._router;
	}

	get t(): TranslationPathsService {
		return this._t;
	}

	get lang(): string | undefined {
		return this._lang;
	}

	get routes(): {
		builder: (resumeId: string) => { templates: string; builder: string };
		dashboard: { main: string; payment: string; profile: string };
		facade: LandingModuleRoutesInterface;
	} {
		return this._routes;
	}

	async switchLanguage(lang: LangType): Promise<void> {
		this._translateService.use(lang);
		localStorage.setItem('lang', lang);

		await this._authService.setLanguageAPI();
	}

	toggleDropdown(unfold = false): void {
		if (unfold) {
			this.isDroppedDown = false;

			if (this.backDrop) {
				document.body.removeChild(this.backDrop);
				this.backDrop = undefined;
			}

			return;
		}
		this.isDroppedDown = !this.isDroppedDown;
		if (this.isDroppedDown) {
			this.backDrop = document.createElement('DIV');
			this.backDrop.classList.add('nav-dropdown-backdrop');
			document.body.appendChild(this.backDrop);

			this.backDrop.addEventListener('click', () => {
				this.toggleDropdown();
			});
		}

		if (!this.isDroppedDown && this.backDrop) {
			document.body.removeChild(this.backDrop);
			this.backDrop = undefined;
		}
	}

	isRTL(): boolean {
		return localStorage.getItem('lang') === 'ar';
	}

	async logout(): Promise<void> {
		console.log('logging out');
		this._loaderService.showLoader();
		try {
			await this._authService?.logoutAPI();
			this._loaderService.hideLoader();
			location.href = environment.mainURL;
		} catch (e) {
			console.error(e);
			this._loaderService.hideLoader();
		}
	}
}
