import { AfterViewChecked, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { BaseEditableContent } from '../../classes/base-editable-content.class';

@Component({
	selector: 'blackbird-editable-h1',
	templateUrl: './editable-h1.component.html',
	styleUrls: ['./editable-h1.component.scss']
})
export class EditableH1Component extends BaseEditableContent implements AfterViewChecked {
	@ViewChild('editableElement') editableElement!: ElementRef<HTMLElement>;
	@Output() titleChange: EventEmitter<string> = new EventEmitter<string>();

	@Input() title = '';

	private _titleChangeTimeout: unknown;

	ngAfterViewChecked(): void {
		if (this.editableElement !== this.contentEditable) {
			this.contentEditable = this.editableElement;
		}
	}

	handleChange(ev: Event): void {
		if (this._titleChangeTimeout) {
			// eslint-disable-next-line @typescript-eslint/no-unsafe-argument
			clearTimeout(this._titleChangeTimeout as any);
		}

		this._titleChangeTimeout = setTimeout(() => {
			this.titleChange.emit((<HTMLElement>ev.target)?.textContent?.trim());
		}, 1500);
	}

	isRTL(): boolean {
		return localStorage.getItem('lang') === 'ar';
	}
}
