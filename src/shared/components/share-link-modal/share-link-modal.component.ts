import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { SimpleModalComponent } from 'ngx-simple-modal';
import { TranslationPathsService } from '../../../core/services/translation-paths.service';
import { ClassicResumePreviewType } from '../../../types/classic-resume.types';
import { environment } from '../../../environments/environment';
import { Clipboard } from '@angular/cdk/clipboard';
import { toast } from 'lets-toast';
import { TranslateService } from '@ngx-translate/core';
import { firstValueFrom, Observable } from 'rxjs';

@Component({
	selector: 'blackbird-share-link-modal',
	templateUrl: './share-link-modal.component.html',
	styleUrls: ['./share-link-modal.component.scss']
})
export class ShareLinkModalComponent
	extends SimpleModalComponent<Pick<ClassicResumePreviewType, 'sharingLink'>, void>
	implements OnInit, OnDestroy, Pick<ClassicResumePreviewType, 'sharingLink'>
{
	@ViewChild('copyLinkInput') copyLinkInput!: ElementRef<HTMLInputElement>;
	sharingLink!: string;

	isFocused = false;
	cvURL = '';
	urlCopiedNotifText = '';

	constructor(
		private _t: TranslationPathsService,
		private _clipboard: Clipboard,
		private _translateService: TranslateService
	) {
		super();
	}

	get t(): TranslationPathsService {
		return this._t;
	}

	private async _getTranslatedText(): Promise<void> {
		this.urlCopiedNotifText = await firstValueFrom<string>(
			this._translateService.get(this._t.common.misc.copiedToClipboard) as Observable<string>
		);
	}

	copyURLTpClipboard(): void {
		const isCopied = this._clipboard.copy(this.cvURL);

		if (isCopied) {
			toast(this.urlCopiedNotifText, { severity: 'info', delay: 2000 });
		}
	}

	isRTL(): boolean {
		return localStorage.getItem('lang') === 'ar';
	}

	ngOnInit(): void {
		void this._getTranslatedText();
		setTimeout(() => {
			this.copyLinkInput.nativeElement.addEventListener('select', () => {
				this.copyURLTpClipboard();
			});

			this.cvURL = `${environment.mainURL}/s/${this.sharingLink}`;
		});
	}

	ngOnDestroy(): void {
		if (this.copyLinkInput) {
			this.copyLinkInput.nativeElement.removeEventListener('focus', () => console.info('ev removed'));
			this.copyLinkInput.nativeElement.removeEventListener('blur', () => console.info('ev removed'));
			this.copyLinkInput.nativeElement.removeEventListener('select', () => console.info('ev removed'));
		}
	}
}
