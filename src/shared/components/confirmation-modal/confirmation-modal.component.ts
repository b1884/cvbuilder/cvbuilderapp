import { Component } from '@angular/core';
import { SimpleModalComponent } from 'ngx-simple-modal';

export interface ConfirmationModalInterface {
	title: string;
	description: string;
	confirmationText: string;
	cancelText: string;
	reverseButtons?: boolean;
}

@Component({
	selector: 'blackbird-confirmation-modal',
	templateUrl: './confirmation-modal.component.html',
	styleUrls: ['./confirmation-modal.component.scss']
})
export class ConfirmationModalComponent
	extends SimpleModalComponent<ConfirmationModalInterface, boolean>
	implements ConfirmationModalInterface
{
	cancelText!: string;
	confirmationText!: string;
	description!: string;
	title!: string;
	reverseButtons = false;

	constructor() {
		super();
	}

	isRTL(): boolean {
		return localStorage.getItem('lang') === 'ar';
	}
}
