import { Component } from '@angular/core';

@Component({
	selector: 'blackbird-public-nav-bar',
	templateUrl: './public-nav-bar.component.html',
	styleUrls: ['./public-nav-bar.component.scss']
})
export class PublicNavBarComponent {}
