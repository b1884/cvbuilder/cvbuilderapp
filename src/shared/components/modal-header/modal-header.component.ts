import { Component, EventEmitter, Output } from '@angular/core';

@Component({
	selector: 'blackbird-modal-header',
	templateUrl: './modal-header.component.html',
	styleUrls: ['./modal-header.component.scss']
})
export class ModalHeaderComponent {
	@Output() closeEvent: EventEmitter<void> = new EventEmitter<void>();

	closeModal(): void {
		this.closeEvent.emit();
	}
}
