import { AfterViewChecked, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { BaseEditableContent } from '../../classes/base-editable-content.class';

@Component({
	selector: 'blackbird-editable-h2',
	templateUrl: './editable-h2.component.html',
	styleUrls: ['./editable-h2.component.scss']
})
export class EditableH2Component extends BaseEditableContent implements AfterViewChecked {
	@ViewChild('editableElement') editableElement!: ElementRef<HTMLElement>;
	@Output() titleChange: EventEmitter<string> = new EventEmitter<string>();

	@Input() title = '';
	@Input() isDraggable!: boolean | undefined;

	private _titleChangeTimeout: unknown;

	ngAfterViewChecked(): void {
		if (this.editableElement !== this.contentEditable) {
			this.contentEditable = this.editableElement;
		}
	}

	handleChange(ev: Event): void {
		if (this._titleChangeTimeout) {
			// eslint-disable-next-line @typescript-eslint/no-unsafe-argument
			clearTimeout(this._titleChangeTimeout as any);
		}

		this._titleChangeTimeout = setTimeout(() => {
			this.titleChange.emit((<HTMLElement>ev.target)?.textContent?.trim());
		}, 1500);
	}

	isRTL(): boolean {
		return localStorage.getItem('lang') === 'ar';
	}
}
