import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicNavBarComponent } from './components/public-nav-bar/public-nav-bar.component';
import { ShareLinkModalComponent } from './components/share-link-modal/share-link-modal.component';
import { ConfirmationModalComponent } from './components/confirmation-modal/confirmation-modal.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { pluck } from 'rxjs/operators';
import { asapScheduler, Observable, scheduled } from 'rxjs';
import lng from '../locals/fr';
import { ModalHeaderComponent } from './components/modal-header/modal-header.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { EditableH1Component } from './components/editable-h1/editable-h1.component';
import { EditableH2Component } from './components/editable-h2/editable-h2.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxPopperModule } from 'ngx-popper';

export class WebpackTranslateLoader implements TranslateLoader {
	getTranslation(lang: string): Observable<typeof lng> {
		// eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
		return scheduled<typeof lng>(import(`../locals/${lang}/index.ts`), asapScheduler).pipe(
			pluck('default')
		) as Observable<typeof lng>;
	}
}

@NgModule({
	declarations: [
		PublicNavBarComponent,
		ShareLinkModalComponent,
		ConfirmationModalComponent,
		ModalHeaderComponent,
		EditableH1Component,
		EditableH2Component
	],
	imports: [
		CommonModule,
		CarouselModule,
		HttpClientModule,
		ReactiveFormsModule,
		NgxPopperModule.forRoot({
			disableDefaultStyling: true,
			applyClass: 'popper-tooltip-styles'
		}),
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useClass: WebpackTranslateLoader
			}
		})
	],
	exports: [
		PublicNavBarComponent,
		ReactiveFormsModule,
		ShareLinkModalComponent,
		ConfirmationModalComponent,
		NgxPopperModule,
		TranslateModule,
		ModalHeaderComponent,
		CarouselModule,
		EditableH1Component,
		EditableH2Component
	]
})
export class SharedModule {}
