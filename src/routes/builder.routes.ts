export default (mainPath: string) =>
	(resumeId: string): { templates: string; builder: string } => ({
		builder: `${mainPath}/${resumeId}`,
		templates: `${mainPath}/${resumeId}/preview`
	});
