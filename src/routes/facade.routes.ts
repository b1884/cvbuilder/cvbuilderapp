export interface LandingModuleRoutesInterface {
	home: string;
	termsFr: string;
	faqFr: string;
	privacyFr: string;
	aboutFr: string;
	templatesFr: string;
	pricingFr: string;
}

const getLanguageSpecificURL = (frURL: string, enURL: string, lang?: string | null, arURL?: string): string => {
	if (!lang) {
		return frURL;
	}

	switch (lang) {
		case 'fr':
			return frURL;
		case 'en':
			return enURL;
		case 'ar':
			if (!arURL) {
				return frURL;
			}

			return arURL;
		default:
			return frURL;
	}
};

export default (mainPath: string): LandingModuleRoutesInterface => ({
	home: `${mainPath}/${getLanguageSpecificURL('', 'cv-template', localStorage.getItem('lang'), 'سيرة-ذاتية')}`,
	termsFr: `${mainPath}/${getLanguageSpecificURL(
		'conditions-generales',
		'terms-of-service',
		localStorage.getItem('lang'),
		'شروط-الاستخدام'
	)}`,
	faqFr: `${mainPath}/${getLanguageSpecificURL(
		'faire-un-cv',
		'how-to-make-cv',
		localStorage.getItem('lang'),
		'تصميم-سيرة-ذاتية'
	)}`,
	privacyFr: `${mainPath}/${getLanguageSpecificURL(
		'politique-confidentialite',
		'privacy-policy',
		localStorage.getItem('lang'),
		'سياسة-الخصوصية'
	)}`,
	aboutFr: `${mainPath}/${getLanguageSpecificURL('a-propos', 'about-us', localStorage.getItem('lang'), 'من-نحن')}`,
	templatesFr: `${mainPath}/${getLanguageSpecificURL(
		'modele-cv',
		'cv-examples',
		localStorage.getItem('lang'),
		'نماذج-سيرة-ذاتية'
	)}`,
	pricingFr: `${mainPath}/${getLanguageSpecificURL('tarifs', 'pricing', localStorage.getItem('lang'), 'التعريفة')}`
});
