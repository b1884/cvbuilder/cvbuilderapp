export default (mainPath: string): { main: string; payment: string; profile: string } => ({
	main: `${mainPath}`,
	payment: `${mainPath}get-hired-now`,
	profile: `${mainPath}profile`
});
