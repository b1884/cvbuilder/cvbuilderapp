import dash from './dashboard.routes';
import builder from './builder.routes';
import facade from './facade.routes';
import { environment } from '../environments/environment';

export default {
	dashboard: dash('/'),
	builder: builder('/builder'),
	facade: facade(environment.mainURL)
};
