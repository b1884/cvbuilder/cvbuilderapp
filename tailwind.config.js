module.exports = {
  prefix: '',
  purge: {
    enabled: process.env.NODE_ENV === 'production',
    content: ['./src/**/*.{html,ts}']
  },
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    minWidth: {
      'screen-container': '1080px',
      'full': '100%',
      '90-percent': '90%',
      '95-percent': '95%',
      '98-percent': '98%'
    },
    screens: {
      mdCustom: '888px'
    },
    boxShadow: {
      'cvbuilder-none': 'none',
      'cvbuilder-button': '1px 1px 4px rgba(0, 0, 0, 0.25)',
      'cvbuilder-drop-image': '1px 2px 20px rgba(0, 0, 0, 0.25)',
      'cvbuilder-drop': '0 2px 3px .5px rgba(0, 0, 0, 0.25)',
      'cvbuilder-line': '0 .5px 0px 0px rgba(0, 0, 0, 0.25)',
      'cvbuilder-border': '0px 0px 0px 0.5px rgba(0, 0, 0, 0.25)'
    },
    fontFamily: {
      sans: ['Poppins', 'sans-serif'],
      display: ['Poppins', 'sans-serif'],
      body: ['Poppins', 'sans-serif']
    },
    colors: {
      transparent: 'rgba(0, 0, 0, 0)',
      primary: '#9529ff',
      'primary-10': 'rgba(149,41,255,0.15)',
      secondary: '#FF0AA9',
      'secondary-10': 'rgba(255,10,169,0.15)',
      black: '#000',
      dark: '#130524',
      'almost-gray': 'rgba(228, 230, 235, 0.6)',
      'less-dark': '#A7C1D9',
      danger: '#ff193b',
      bgZero: '#F7F8FA',
      bgOne: 'rgba(196,196,196, .2)',
      bgThree: 'rgba(196,196,196, .6)',
      bgFour: '#DCDEE1',
      bgFive: 'rgba(220,222,225,0.4)',
      bgTwo: '#717A8F',
      'black-50': 'rgba(0, 0, 0, .5)',
      'black-40': 'rgba(0, 0, 0, .4)',
      'black-30': 'rgba(0, 0, 0, .3)',
      'black-25': 'rgba(0, 0, 0, .25)',
      'black-20': 'rgba(0, 0, 0, .2)',
      'black-10': 'rgba(0, 0, 0, .1)',
      'white-50': 'rgba(255, 255, 255, .5)',
      'white-40': 'rgba(255, 255, 255, .4)',
      'white-30': 'rgba(255, 255, 255, .3)',
      'white-20': 'rgba(255, 255, 255, .2)',
      'white-10': 'rgba(255, 255, 255, .1)',
      white: '#fff',
      whitesmoke: 'whitesmoke'
    },
    extend: {
      height: function(theme) {
        return {
          '98/2': '12.5rem',
          '98': '25rem',
          '70': '17.359rem',
          '160': '40rem',
          'screen-42': '42vh',
          'screen-63': '63vh',
          'screen-37': '37vh',
          'screen-70': '70vh',
          'screen-80': '80vh',
          'screen/2': '50vh',
          'screen/3': 'calc(100vh / 3)',
          'screen/4': 'calc(100vh / 4)',
          'screen/5': 'calc(100vh / 5)'
        };
      },
      width: function(theme) {
        return {
          'lg': '32rem',
          '65': '16.875rem',
          '98': '25rem',
          '70': '17.359rem',
          '70/2': '8.6795rem',
          'screen-container': '1080px',
          '160': '40rem',
          '98-percent': '98%',
          '95-percent': '95%',
          '70-percent': '70%',
          '65-percent': '65%',
          '60-percent': '60%',
          '40-percent': '40%',
          '48-percent': '48%',
          'screen-95': '95vw',
          'screen/2': '50vw',
          'screen/4/5': '80vw',
          'screen/7/12': '58.333333vw',
          'screen/8/12': '66.6666666666vw',
          'screen/2-spacing-10': 'calc(50vw - 10px)',
          'screen/2-spacing-15': 'calc(50vw - 15px)',
          'screen/2-spacing-20': 'calc(50vw - 20px)',
          'screen/2-spacing-30': 'calc(50vw - 30px)',
          'screen/3': 'calc(100vw / 3)',
          'screen/3-spacing-12': 'calc((100vw / 3) - 12px)',
          'screen/3-spacing-50': 'calc((100vw / 3) - 50px)',
          'screen/3-spacing-40': 'calc((100vw / 3) - 40px)',
          'screen/4': 'calc(100vw / 4)',
          'screen/5': 'calc(100vw / 5)',
          'screen/5-spacing-20': 'calc((100vw / 5) - 20px)',
          'screen/5-spacing-30': 'calc((100vw / 5) - 30px)',
          'screen/7-spacing-30': 'calc((100vw / 7) - 30px)',
          'screen/9-spacing-30': 'calc((100vw / 9) - 30px)'
        };
      }
    }
  },
  variants: {
    extend: {}
  },
  plugins: [require('@tailwindcss/typography')]
};
